package com.simp.newmeetingroom.Fragments;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.simp.newmeetingroom.Adapter.AdapterAttendees;
import com.simp.newmeetingroom.Common.DateLibs;
import com.simp.newmeetingroom.Model.Attendee;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;
import com.simp.newmeetingroom.R;

import java.util.ArrayList;
import java.util.List;

public class Dialog_DetailEvent_Fragment extends DialogFragment {
    public static final String SENDDATADETAILEVENT = "DATABUNDLE";
    public static final String SENDDATASTRING = "DATASTRING";
    public static final String EVENTTODAY = "EVENTTODAY";
    ImageView imgClose;
    RecyclerView recyclerView;
    AdapterAttendees adapterAttendees;
    List<Attendee>attendeeList = new ArrayList<Attendee>();
    Event events = null;
    EventNext eventNext = null;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("API123", "onCreate");
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_eventdetail, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final EditText editText = view.findViewById(R.id.inPIN);
        final String[]DataEvents = getArguments().getStringArray(SENDDATASTRING);
        if(getArguments().getBoolean(EVENTTODAY)){
            events = getArguments().getParcelable(SENDDATADETAILEVENT);
            TextView eventSummary = view.findViewById(R.id.eventTitle);
            TextView eventDate = view.findViewById(R.id.eventDate);
            TextView eventDuration = view.findViewById(R.id.eventDuration);
            recyclerView = view.findViewById(R.id.item_list);
            attendeeList = events.getAttendees();
            adapterAttendees = new AdapterAttendees(this, events.getAttendees());
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapterAttendees);
            adapterAttendees.notifyDataSetChanged();
            TextView eventDescription = view.findViewById(R.id.EventDesc);
            TextView eventOrganizer = view.findViewById(R.id.EventOrganizer);
            eventSummary.setText(events.getSummary());
            eventOrganizer.setText(events.getOrganizerEmail());
            String Description = events.getDescription();
            if(Description==null){
                eventDescription.setText(Html.fromHtml(""));
            }else{
                eventDescription.setText(Html.fromHtml(events.getDescription()));
            }

            String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(events.getStartDateTime());
            String TanggalMulai = null;
            // DATE//
            if(DateLibs.CekEventToday(events.getStartDateTime())){
                TanggalMulai = "Today";
            }else{
                TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
            }
            String tanggal = DateLibs.getTgl(events.getStartDateTime());
            String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
            String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
            String JamMulai = DateLibs.getJam(events.getStartDateTime());
            String MenitMulai = DateLibs.getMinutes(events.getStartDateTime());
            String JamSelesai = DateLibs.getJam(events.getEndDateTime());
            String MenitSelesai = DateLibs.getMinutes(events.getEndDateTime());
            String MulaiDate = DateLibs.getTanggalandTime(events.getStartDateTime());
            String SelesaiDate = DateLibs.getTanggalandTime(events.getEndDateTime());
            String duration = DateLibs.HitungDUration(MulaiDate,SelesaiDate);
            eventDate.setText(TanggalMulai+", "+JamMulai+":"+MenitMulai+", "+MonthMulai+" "+tanggal);
            eventDuration.setText(duration);
            // DATE//
            imgClose = view.findViewById(R.id.btnClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    DialogDetailEventListener dialogListener = (DialogDetailEventListener) getActivity();
                    String INPUTTEXT = "";
                    dialogListener.onDialogEventDetail(INPUTTEXT,DataEvents,events,eventNext);

                }
            });
        }else{
            eventNext = getArguments().getParcelable(SENDDATADETAILEVENT);
            TextView eventSummary = view.findViewById(R.id.eventTitle);
            TextView eventDate = view.findViewById(R.id.eventDate);
            TextView eventDuration = view.findViewById(R.id.eventDuration);
            recyclerView = view.findViewById(R.id.item_list);
            attendeeList = eventNext.getAttendees();
            adapterAttendees = new AdapterAttendees(this, eventNext.getAttendees());
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapterAttendees);
            adapterAttendees.notifyDataSetChanged();
            TextView eventDescription = view.findViewById(R.id.EventDesc);
            TextView eventOrganizer = view.findViewById(R.id.EventOrganizer);
            eventSummary.setText(eventNext.getSummary());
            eventOrganizer.setText(eventNext.getOrganizerEmail());
            eventDescription.setText(Html.fromHtml(eventNext.getDescription()));
            String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(eventNext.getStartDateTime());
            String TanggalMulai = null;
            // DATE//
            if(DateLibs.CekEventToday(eventNext.getStartDateTime())){
                TanggalMulai = "Today";
            }else{
                TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
            }
            String tanggal = DateLibs.getTgl(eventNext.getStartDateTime());
            String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
            String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
            String JamMulai = DateLibs.getJam(eventNext.getStartDateTime());
            String MenitMulai = DateLibs.getMinutes(eventNext.getStartDateTime());
            String JamSelesai = DateLibs.getJam(eventNext.getEndDateTime());
            String MenitSelesai = DateLibs.getMinutes(eventNext.getEndDateTime());
            long WaktuEnding = DateLibs.getDateTimeByFromEvent(eventNext.getEndDateTime());
            long WaktuMulai = DateLibs.getDateTimeByFromEvent(eventNext.getStartDateTime());
            long TOtalWaktu = WaktuEnding-WaktuMulai;
            String MulaiDate = DateLibs.getTanggalandTime(eventNext.getStartDateTime());
            String SelesaiDate = DateLibs.getTanggalandTime(eventNext.getEndDateTime());
            String duration = DateLibs.HitungDUration(MulaiDate,SelesaiDate);
            eventDate.setText(TanggalMulai+", "+JamMulai+":"+MenitMulai+", "+MonthMulai+" "+tanggal);
            eventDuration.setText(duration);
            // DATE//

            imgClose = view.findViewById(R.id.btnClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    DialogDetailEventListener dialogListener = (DialogDetailEventListener) getActivity();
                    String INPUTTEXT = "";
                    dialogListener.onDialogEventDetail(INPUTTEXT,DataEvents,events,eventNext);

                }
            });
        }


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface DialogDetailEventListener {
        void onDialogEventDetail(String inputText, String[] DataEvents, Event eventDetail,EventNext eventNext);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


}