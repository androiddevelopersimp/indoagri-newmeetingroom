package com.simp.newmeetingroom.Fragments;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.simp.newmeetingroom.R;

public class Dialog_Push_Fragment extends DialogFragment {

    EditText editPin;
    ImageView imgClose;
    Button btnangka1,btnangka2,btnangka3,btnangka4,btnangka5,btnangka6,btnangka7,btnangka8,btnangka9,btnangka0,btnreset,btnenter;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("API123", "onCreate");
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_check_push, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final EditText editText = view.findViewById(R.id.inPIN);
        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("email")))
            editText.setText(getArguments().getString("email"));
        final String[]DataEvents = getArguments().getStringArray("data");
        imgClose = view.findViewById(R.id.btnClose);
        editPin = view.findViewById(R.id.edittext_pin);
        btnangka0 = (Button)view.findViewById(R.id.angka0);
        btnangka1 = (Button)view.findViewById(R.id.angka1);
        btnangka2 = (Button)view.findViewById(R.id.angka2);
        btnangka3 = (Button)view.findViewById(R.id.angka3);
        btnangka4 = (Button)view.findViewById(R.id.angka4);
        btnangka5 = (Button)view.findViewById(R.id.angka5);
        btnangka6 = (Button)view.findViewById(R.id.angka6);
        btnangka7 = (Button)view.findViewById(R.id.angka7);
        btnangka8 = (Button)view.findViewById(R.id.angka8);
        btnangka9 = (Button)view.findViewById(R.id.angka9);
        btnreset= (Button)view.findViewById(R.id.reset);
        Button btnDone = view.findViewById(R.id.btnDone);
        final String NumberPin = editPin.getText().toString();
        btnangka0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"0");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnangka1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"1");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"2");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"3");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"4");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"5");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"6");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"7");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"8");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnangka9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editPin.getText().toString().length()<4){
                    editPin.setText(editPin.getText()+"9");
                }else{
                    Toast.makeText(getDialog().getContext(), "Over Limit Pin Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPin.getText().clear();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogListener dialogListener = (DialogListener) getActivity();
                String PIN = editPin.getText().toString();
                dialogListener.onDialogPinNumber(PIN,DataEvents);
                dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                DialogListener dialogListener = (DialogListener) getActivity();
                String PIN = "";
                dialogListener.onDialogPinNumber(PIN,DataEvents);

            }
        });
        Button enterPin = view.findViewById(R.id.enter);
        enterPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                String PIN = editPin.getText().toString();
                DialogListener dialogListener = (DialogListener) getActivity();
                dialogListener.onDialogPinNumber(PIN,DataEvents);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface DialogListener {
        void onDialogPinNumber(String inputText, String[] DataEvents);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

}