package com.simp.newmeetingroom.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;
import com.simp.newmeetingroom.R;

import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.RESULT_CODE;

public class ThisRoomFragment extends Fragment {

    public static final String SCOPE = "SCOPE";
    public static final String SENDDATA = "DATABUNDLE";
    /**
     * The dummy content this fragment is presenting.
     */
    // private DummyContent.DummyItem mItem;

    private Event events;
    private EventNext eventNext;
    Bundle bundle;
    int TypeService;
    int ResultCode;
    String [] DATA;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    TextView summaryText;

    private OnFragmentInteractionListener mListener;

    public ThisRoomFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle = this.getArguments();
            TypeService = bundle.getInt(PAR_TYPE_SERVICE);
            DATA = bundle.getStringArray(PAR_DATA_SERVICE);
            ResultCode = bundle.getInt(RESULT_CODE);
            if(bundle.getString(SCOPE).equals("Today")){
                events = bundle.getParcelable(SENDDATA);
            }else{
                eventNext = bundle.getParcelable(SENDDATA);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView;
        rootView = inflater.inflate(R.layout.layout_item_detail, container, false);

        if(TypeService == 0){
            rootView.setBackgroundResource(R.color.colorPrimary);
        }else{
            rootView.setBackgroundResource(R.color.colorBase);
        }
//        TextView textView = rootView.findViewById(R.id.txtTitle);
//        textView.setText(Status);
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(int StatusTypeListener, String[] Data) {
        if (mListener != null) {
            mListener.onFragmentInteraction(StatusTypeListener,Data);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int StatusTypeListener, String[] data);
    }
}
