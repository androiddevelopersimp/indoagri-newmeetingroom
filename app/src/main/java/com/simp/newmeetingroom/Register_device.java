package com.simp.newmeetingroom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.simp.newmeetingroom.Common.DeviceUtils;
import com.simp.newmeetingroom.Data.SharePreference;
import com.simp.newmeetingroom.Network.APIServices;
import com.simp.newmeetingroom.Network.NetClient;
import com.simp.newmeetingroom.Network.Response.Hardware;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register_device extends AppCompatActivity {
    private static final String TAG = "LOGIN";
    APIServices apiServices;
    ProgressBar progressBar;
    Hardware hardware;
    com.simp.newmeetingroom.Model.Hardware hardwareModel;
    SharePreference sharePreference;
    boolean isLogin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_device);
        apiServices = NetClient.AuthProduction().create(APIServices.class);
        progressBar = findViewById(R.id.progressBar);
        sharePreference = new SharePreference(Register_device.this);
        isLogin = sharePreference.isLoggedIn();
        initLogin();
    }

    public void initLogin(){
        progressBar.setVisibility(View.VISIBLE);
        if(isLogin){
            progressBar.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent a = new Intent(Register_device.this, ItemListHomeActivity.class);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(a);
                }
            }, 3000);
        }else{
           progressBar.setVisibility(View.GONE);
           // GetuserForLogin(getApplicationContext());
        }
    }

    public void btnRegister(View view){
        progressBar.setVisibility(View.VISIBLE);
        GetuserForLogin(Register_device.this);
    }

    private void GetuserForLogin(Context context){
        postLogin().enqueue(new Callback<com.simp.newmeetingroom.Model.Hardware>() {
            @Override
            public void onResponse(Call<com.simp.newmeetingroom.Model.Hardware> call, Response<com.simp.newmeetingroom.Model.Hardware> response) {
               progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    Log.i(TAG,response.body().toString());
                    // Toast.makeText(getApplicationContext(),"Berhasil",Toast.LENGTH_SHORT).show();
                    hardwareModel = fetchResults(response);
                    Toast.makeText(getApplicationContext(), String.valueOf(hardwareModel.getHardKey()), Toast.LENGTH_SHORT).show();
                    sharePreference.setKeyHardkey(hardwareModel.getHardKey());
                    sharePreference.setKeyRoomid(hardwareModel.getRoomId());
                    sharePreference.setKeyHardserial(hardwareModel.getHardSerial());
                    sharePreference.setKeyHardbarcode(hardwareModel.getHardBarcode());
                    sharePreference.setKeyHardname(hardwareModel.getHardName());
                    sharePreference.setKeyHardpassword(hardwareModel.getHardPassword());
                    sharePreference.setKeyHardmanufacture(hardwareModel.getHardManufacture());
                    sharePreference.setKeyHardbrand(hardwareModel.getHardBrand());
                    sharePreference.setKeyHardmodel(hardwareModel.getHardModel());
                    sharePreference.setKeyTimeoff(hardwareModel.getTimeOff());
                    sharePreference.setKeyTimeon(hardwareModel.getTimeOn());
                    sharePreference.setLogin(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent a = new Intent(Register_device.this, ItemListHomeActivity.class);
                            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(a);
                        }
                    }, 3000);

                }else{
                    Toast.makeText(getApplicationContext(),"gagal Login",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<com.simp.newmeetingroom.Model.Hardware> call, Throwable t) {
                t.printStackTrace();
                Log.i(TAG,t.getMessage());
            }
        });

    }

    private com.simp.newmeetingroom.Model.Hardware fetchResults(Response<com.simp.newmeetingroom.Model.Hardware> response) {
        com.simp.newmeetingroom.Model.Hardware token = response.body();
        return token;
    }
    private Call<com.simp.newmeetingroom.Model.Hardware> postLogin() {
        String device = DeviceUtils.getDeviceID(Register_device.this);
        //String device = "359143063342658";
        return apiServices.Login(device);
    }

}
