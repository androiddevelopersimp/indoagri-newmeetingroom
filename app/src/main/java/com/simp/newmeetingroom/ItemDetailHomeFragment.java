package com.simp.newmeetingroom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.simp.newmeetingroom.Common.DateLibs;
import com.simp.newmeetingroom.Model.Attendee;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.view.animation.Animation.RELATIVE_TO_SELF;
import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.RESULT_CODE;
import static com.simp.newmeetingroom.Common.Constants.StatusListenerThisRoom;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomAttendees;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomCheckIn;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomCheckOut;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListHomeActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailHomeActivity}
 * on handsets.
 */
public class ItemDetailHomeFragment extends Fragment{
    private static final String TAG = ItemDetailHomeFragment.class.getSimpleName();
    public static final String SCOPE = "SCOPE";
    public static final String SENDDATA = "DATABUNDLE";
    private Event events = null;
    private EventNext eventNext = null;
    Bundle bundle;
    int TypeService;
    int ResultCode;
    String [] DATA;
     private OnFragmentInteractionListener mListener;
    TextView summaryText;
    TextView timeTxt;
    long TimeNow;
    long TimeEvent;
    Activity mActivity;

    int myProgress = 0;
    ProgressBar progressBarView;
    Button btn_start;
    TextView tv_time;
    int progress;
    CountDownTimer countDownTimer;
    int endTime = 0;
    Button btnDetailEvent;
    TextView totalAttendee;
    public ItemDetailHomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(SCOPE) && getArguments().containsKey(SENDDATA)) {
            bundle = this.getArguments();
            TypeService = bundle.getInt(PAR_TYPE_SERVICE);
            DATA = bundle.getStringArray(PAR_DATA_SERVICE);
            ResultCode = bundle.getInt(RESULT_CODE);
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                if(bundle.getParcelable(SENDDATA) !=null){
                    if(bundle.getString(SCOPE).equals("Today")){
                        events = bundle.getParcelable(SENDDATA);
                        TimeNow = DateLibs.getDateTime();
                        TimeEvent = DateLibs.getDateTimeByFromEvent(events.getStartDateTime());
                    }else{
                        eventNext = bundle.getParcelable(SENDDATA);
                        TimeNow = DateLibs.getDateTime();
                        TimeEvent = DateLibs.getDateTimeByFromEvent(eventNext.getStartDateTime());
                    }
                }
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = null;
            if(bundle.getString(SCOPE).equals("Today")){
                if(DATA[0].equalsIgnoreCase("BUFFERING")){
                    rootView = LoadingStart(rootView,inflater,container,savedInstanceState);
                    return rootView;
                } if(DATA[0].equalsIgnoreCase("ROUTINE")){
                    events = bundle.getParcelable(SENDDATA);
                    if(events==null){
                        rootView = AvailableSession(rootView,inflater,container,savedInstanceState);
                        return rootView;
                    }
                    if(events!=null){
                        if(DATA[1].equalsIgnoreCase("AVAILABLE")){
                            rootView = AvailableSession(rootView,inflater,container,savedInstanceState);
                            return rootView;
                        }else{
                            String Today = DateLibs.getToday();
                            String IdleTimes = DateLibs.getWaktuIdle(events.getStartDateTime());
                            long WaktuNow = DateLibs.getDateTimeBy(Today);
                            long WaktuIdle = DateLibs.getDateTimeByFromEvent(IdleTimes);
                            long WaktuEnding = DateLibs.getDateTimeByFromEvent(events.getEndDateTime());
                            long WaktuMulai = DateLibs.getDateTimeByFromEvent(events.getStartDateTime());
                            long SisaIdle = WaktuIdle - WaktuMulai;
                            long TotalWaktuEvent = WaktuEnding - WaktuMulai;
                            long SisaWaktuEvent = WaktuEnding - WaktuNow;
                            if(WaktuNow < WaktuMulai){
                                if(events.getEventStatus().equalsIgnoreCase("1"))
                                {
                                    rootView = WaitingSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }if(events.getEventStatus().equalsIgnoreCase("2"))
                                {
                                    rootView = BookSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }
                            }if(WaktuNow > WaktuMulai){
                                if(events.getEventStatus().equalsIgnoreCase("1"))
                                {
                                    rootView = WaitingSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }if(events.getEventStatus().equalsIgnoreCase("2"))
                                {
                                    rootView = VacantSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }
                                return rootView;
                            }if(WaktuNow < WaktuEnding){
                                if(events.getEventStatus().equalsIgnoreCase("1"))
                                {
                                    rootView = WaitingSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }if(events.getEventStatus().equalsIgnoreCase("2"))
                                {
                                    rootView = VacantSession(rootView,inflater,container,savedInstanceState);
                                    return rootView;
                                }
                            }else{
                                rootView = LoadingStart(rootView,inflater,container,savedInstanceState);
                                return rootView;
                            }
                        }

                    }
                }
            }
            if(bundle.getString(SCOPE).equals("Next")){
                eventNext = bundle.getParcelable(SENDDATA);
                rootView = NextSession(rootView,inflater,container,savedInstanceState);
                return rootView;
            }
        return rootView;
    }


    private View BookSession(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_booked, container, false);
        root.setBackgroundResource(R.color.Booked_Red);
        Button btnPush = (Button)root.findViewById(R.id.btnPush);
        btnDetailEvent = (Button)root.findViewById(R.id.btn_Detail);
        totalAttendee = (TextView)root.findViewById(R.id.totalAttendees);
        TextView txtTime = (TextView)root.findViewById(R.id.txtTime);
        TextView txtHourandMinute = (TextView)root.findViewById(R.id.HourandMinute);
        TextView txtDay = (TextView)root.findViewById(R.id.Daysvent);
        TextView txtMonth = (TextView)root.findViewById(R.id.Monthevent);
        TextView txtYear = (TextView)root.findViewById(R.id.Yearevent);
        LinearLayout layoutTimer = (LinearLayout)root.findViewById(R.id.layoutTimer);
        TextView txtEventDesc = (TextView)root.findViewById(R.id.descriptionEvent);
        TextView txtOrganizer = (TextView)root.findViewById(R.id.organizerEvent);
        String Description = events.getDescription();
        if(Description==null){
            txtEventDesc.setText(Html.fromHtml(" "));
        }else{
            txtEventDesc.setText(Html.fromHtml(events.getDescription()));
        }
        txtOrganizer.setText(events.getOrganizerFullName());
        final String [] DataSend = new String[6];
        DataSend[0] = String.valueOf(events.getEventId());
        DataSend[1] = events.getRoomEmail();
        DataSend[2] = events.getOrganizerEmail();
        DataSend[3] = events.getPin();
        DataSend[4] = events.getDescription();
        DataSend[5] = "Checkin";
        String Today = DateLibs.getToday();
        String IdleTimes = DateLibs.getWaktuIdle(events.getStartDateTime());
        long WaktuNow = DateLibs.getDateTimeBy(Today);
        long WaktuIdle = DateLibs.getDateTimeByFromEvent(IdleTimes);
        long WaktuEnding = DateLibs.getDateTimeByFromEvent(events.getEndDateTime());
        long WaktuMulai = DateLibs.getDateTimeByFromEvent(events.getStartDateTime());
        long SisaIdle = WaktuIdle - WaktuNow;
        long TotalWaktuEvent = WaktuEnding - WaktuMulai;
        long SisaWaktuEvent = WaktuEnding - WaktuNow;
        String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(events.getStartDateTime());

        String TanggalMulai = null;
        if(DateLibs.CekEventToday(events.getStartDateTime())){
            TanggalMulai = "Today";
        }else{
            TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
        }
        String tanggal = DateLibs.getTgl(events.getStartDateTime());
        String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
        String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
        String JamMulai = DateLibs.getJam(events.getStartDateTime());
        String MenitMulai = DateLibs.getMinutes(events.getStartDateTime());
        String JamSelesai = DateLibs.getJam(events.getEndDateTime());
        String MenitSelesai = DateLibs.getMinutes(events.getEndDateTime());
        txtDay.setText(TanggalMulai+", ");
        txtHourandMinute.setText(JamMulai+":"+MenitMulai);
        txtMonth.setText(MonthMulai+" ");
        txtYear.setText(tanggal);
        btnPush.setEnabled(false);
        layoutTimer.setVisibility(View.GONE);
        String totalAtt = String.valueOf(events.getAttendees().size());
        totalAttendee.setText("+"+totalAtt);
        btnDetailEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomAttendees,events,eventNext);
            }
        });
        btnPush.setText("Waiting");
        btnPush.setBackgroundResource(R.color.transparent);
        btnPush.setVisibility(View.GONE);
        return root;

    }
    private View WaitingSession(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_waiting, container, false);
        Button btnPush = (Button)root.findViewById(R.id.btnPush);
        btnDetailEvent = (Button)root.findViewById(R.id.btn_Detail);
        totalAttendee = (TextView)root.findViewById(R.id.totalAttendees);
        TextView txtTime = (TextView)root.findViewById(R.id.txtTime);
        TextView txtHourandMinute = (TextView)root.findViewById(R.id.HourandMinute);
        TextView txtDay = (TextView)root.findViewById(R.id.Daysvent);
        TextView txtMonth = (TextView)root.findViewById(R.id.Monthevent);
        TextView txtYear = (TextView)root.findViewById(R.id.Yearevent);
        LinearLayout layoutTimer = (LinearLayout)root.findViewById(R.id.layoutTimer);
        TextView txtEventDesc = (TextView)root.findViewById(R.id.descriptionEvent);
        TextView txtOrganizer = (TextView)root.findViewById(R.id.organizerEvent);
        String Description = events.getDescription();
        if(Description==null){
            txtEventDesc.setText(Html.fromHtml(" "));
        }else{
            txtEventDesc.setText(Html.fromHtml(events.getDescription()));
        }
        txtOrganizer.setText(events.getOrganizerFullName());
        final String [] DataSend = new String[6];
        DataSend[0] = String.valueOf(events.getEventId());
        DataSend[1] = events.getRoomEmail();
        DataSend[2] = events.getOrganizerEmail();
        DataSend[3] = events.getPin();
        DataSend[4] = events.getDescription();
        DataSend[5] = "Checkin";
        String Today = DateLibs.getToday();
        String IdleTimes = DateLibs.getWaktuIdle(events.getStartDateTime());
        long WaktuNow = DateLibs.getDateTimeBy(Today);
        long WaktuIdle = DateLibs.getDateTimeByFromEvent(IdleTimes);
        long WaktuEnding = DateLibs.getDateTimeByFromEvent(events.getEndDateTime());
        long WaktuMulai = DateLibs.getDateTimeByFromEvent(events.getStartDateTime());
        long SisaIdle = WaktuIdle - WaktuNow;
        long TotalWaktuEvent = WaktuEnding - WaktuMulai;
        long SisaWaktuEvent = WaktuEnding - WaktuNow;
        String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(events.getStartDateTime());
        String TanggalMulai = null;
        if(DateLibs.CekEventToday(events.getStartDateTime())){
            TanggalMulai = "Today";
        }else{
            TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
        }
        String tanggal = DateLibs.getTgl(events.getStartDateTime());
        String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
        String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
        String JamMulai = DateLibs.getJam(events.getStartDateTime());
        String MenitMulai = DateLibs.getMinutes(events.getStartDateTime());
        String JamSelesai = DateLibs.getJam(events.getEndDateTime());
        String MenitSelesai = DateLibs.getMinutes(events.getEndDateTime());
        txtDay.setText(TanggalMulai+", ");
        txtHourandMinute.setText(JamMulai+":"+MenitMulai);
        txtMonth.setText(MonthMulai+" ");
        txtYear.setText(tanggal);
        if(WaktuNow<WaktuMulai){
            layoutTimer.setVisibility(View.GONE);
        }else{
            layoutTimer.setVisibility(View.VISIBLE);
            Timer(txtTime,SisaIdle);
        }
        String totalAtt = String.valueOf(events.getAttendees().size());
        totalAttendee.setText("+"+totalAtt);
        btnDetailEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomAttendees,events,eventNext);
            }
        });
        btnPush.setText("CheckIn");
        btnPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomCheckIn,events,eventNext);
            }
        });
        root.setBackgroundResource(R.color.Remaining_Yellow);
        return root;
    }
    private View VacantSession(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_vacant, container, false);
        Button btnPush = (Button)root.findViewById(R.id.btnPush);
        btnDetailEvent = (Button)root.findViewById(R.id.btn_Detail);
        totalAttendee = (TextView)root.findViewById(R.id.totalAttendees);
        progressBarView = (ProgressBar) root.findViewById(R.id.view_progress_bar);
        tv_time= (TextView)root.findViewById(R.id.tv_timer);
        /*Animation*/
        RotateAnimation makeVertical = new RotateAnimation(0, -90, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        makeVertical.setFillAfter(true);
        progressBarView.startAnimation(makeVertical);
        progressBarView.setSecondaryProgress(endTime);
        progressBarView.setProgress(0);

        TextView txtTime = (TextView)root.findViewById(R.id.txtTime);
        TextView txtHourandMinute = (TextView)root.findViewById(R.id.HourandMinute);
        TextView txtDay = (TextView)root.findViewById(R.id.Daysvent);
        TextView txtMonth = (TextView)root.findViewById(R.id.Monthevent);
        TextView txtYear = (TextView)root.findViewById(R.id.Yearevent);
        LinearLayout layoutTimer = (LinearLayout)root.findViewById(R.id.layoutTimer);
        TextView txtEventDesc = (TextView)root.findViewById(R.id.descriptionEvent);
        TextView txtOrganizer = (TextView)root.findViewById(R.id.organizerEvent);
        String Description = events.getDescription();
        if(Description==null){
            txtEventDesc.setText(Html.fromHtml(" "));
        }else{
            txtEventDesc.setText(Html.fromHtml(events.getDescription()));
        }
        txtOrganizer.setText(events.getOrganizerFullName());
        final String [] DataSend = new String[6];
        DataSend[0] = String.valueOf(events.getEventId());
        DataSend[1] = events.getRoomEmail();
        DataSend[2] = events.getOrganizerEmail();
        DataSend[3] = events.getPin();
        DataSend[4] = events.getDescription();
        DataSend[5] = "CheckOut";
        String Today = DateLibs.getToday();
        String IdleTimes = DateLibs.getWaktuIdle(events.getStartDateTime());
        long WaktuNow = DateLibs.getDateTimeBy(Today);
        long WaktuIdle = DateLibs.getDateTimeByFromEvent(IdleTimes);
        long WaktuEnding = DateLibs.getDateTimeByFromEvent(events.getEndDateTime());
        long WaktuMulai = DateLibs.getDateTimeByFromEvent(events.getStartDateTime());
        long SisaIdle = WaktuIdle - WaktuNow;
        long TotalWaktuEvent = WaktuEnding - WaktuMulai;
        long SisaWaktuEvent = WaktuEnding - WaktuNow;
        String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(events.getStartDateTime());
        String TanggalMulai = null;
        if(DateLibs.CekEventToday(events.getStartDateTime())){
            TanggalMulai = "Today";
        }else{
            TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
        }
        String tanggal = DateLibs.getTgl(events.getStartDateTime());
        String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
        String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
        String JamMulai = DateLibs.getJam(events.getStartDateTime());
        String MenitMulai = DateLibs.getMinutes(events.getStartDateTime());
        String JamSelesai = DateLibs.getJam(events.getEndDateTime());
        String MenitSelesai = DateLibs.getMinutes(events.getEndDateTime());
        txtDay.setText(TanggalMulai+", ");
        txtHourandMinute.setText(JamMulai+":"+MenitMulai);
        txtMonth.setText(MonthMulai+" ");
        txtYear.setText(tanggal);
        if(WaktuNow<WaktuMulai){
            layoutTimer.setVisibility(View.GONE);
        }else{
            layoutTimer.setVisibility(View.GONE);
            Timer(txtTime,SisaWaktuEvent);
            fn_countdown(SisaWaktuEvent);
        }
        String totalAtt = String.valueOf(events.getAttendees().size());
        totalAttendee.setText("+"+totalAtt);
        btnDetailEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomAttendees,events,eventNext);
            }
        });
        btnPush.setText("CheckOut");
        btnPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomCheckOut,events,eventNext);
            }
        });
        root.setBackgroundResource(R.color.Vacant_Red);
        return root;
    }
    private View AvailableSession(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_available, container, false);
        root.setBackgroundResource(R.color.Available_Green);
        TextView subDesc = root.findViewById(R.id.text_content_description);
        progressBarView = (ProgressBar) root.findViewById(R.id.view_progress_bar);
        tv_time= (TextView)root.findViewById(R.id.tv_timer);
        /*Animation*/
        RotateAnimation makeVertical = new RotateAnimation(0, -90, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        makeVertical.setFillAfter(true);
        progressBarView.startAnimation(makeVertical);
        progressBarView.setSecondaryProgress(endTime);
        progressBarView.setProgress(0);
        subDesc.setText("Book Now Via Email Client");
        return root;
    }
    private View NextSession(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_next, container, false);
        root.setBackgroundResource(R.color.Next_Blue);
        Button btnPush = (Button)root.findViewById(R.id.btnPush);
        btnDetailEvent = (Button)root.findViewById(R.id.btn_Detail);
        totalAttendee = (TextView)root.findViewById(R.id.totalAttendees);
        TextView txtTime = (TextView)root.findViewById(R.id.txtTime);
        TextView txtHourandMinute = (TextView)root.findViewById(R.id.HourandMinute);
        TextView txtDay = (TextView)root.findViewById(R.id.Daysvent);
        TextView txtMonth = (TextView)root.findViewById(R.id.Monthevent);
        TextView txtYear = (TextView)root.findViewById(R.id.Yearevent);
        LinearLayout layoutTimer = (LinearLayout)root.findViewById(R.id.layoutTimer);
        TextView txtEventDesc = (TextView)root.findViewById(R.id.descriptionEvent);
        TextView txtOrganizer = (TextView)root.findViewById(R.id.organizerEvent);
        String Description = eventNext.getDescription();
        if(Description==null){
            txtEventDesc.setText(Html.fromHtml(" "));
        }else{
            txtEventDesc.setText(Html.fromHtml(eventNext.getDescription()));
        }
        txtOrganizer.setText(eventNext.getOrganizerFullName());
        final String [] DataSend = new String[6];
        DataSend[0] = String.valueOf(eventNext.getEventId());
        DataSend[1] = eventNext.getRoomEmail();
        DataSend[2] = eventNext.getOrganizerEmail();
        DataSend[3] = eventNext.getPin();
        DataSend[4] = eventNext.getDescription();
        DataSend[5] = "Checkin";
        String Today = DateLibs.getToday();
        String IdleTimes = DateLibs.getWaktuIdle(eventNext.getStartDateTime());
        long WaktuNow = DateLibs.getDateTimeBy(Today);
        long WaktuIdle = DateLibs.getDateTimeByFromEvent(IdleTimes);
        long WaktuEnding = DateLibs.getDateTimeByFromEvent(eventNext.getEndDateTime());
        long WaktuMulai = DateLibs.getDateTimeByFromEvent(eventNext.getStartDateTime());
        long SisaIdle = WaktuIdle - WaktuNow;
        long TotalWaktuEvent = WaktuEnding - WaktuMulai;
        long SisaWaktuEvent = WaktuEnding - WaktuNow;
        String ConvertTimeStartDateTime = DateLibs.getWaktuIdle(eventNext.getStartDateTime());
        String TanggalMulai = null;
        if(DateLibs.CekEventToday(eventNext.getStartDateTime())){
            TanggalMulai = "Today";
        }else{
            TanggalMulai = DateLibs.getDayOfWeekAbbreviated(ConvertTimeStartDateTime);
        }
        String tanggal = DateLibs.getTgl(eventNext.getStartDateTime());
        String MonthMulai = DateLibs.getMonth(ConvertTimeStartDateTime);
        String YearsMulai = DateLibs.getYear(ConvertTimeStartDateTime);
        String JamMulai = DateLibs.getJam(eventNext.getStartDateTime());
        String MenitMulai = DateLibs.getMinutes(eventNext.getStartDateTime());
        String JamSelesai = DateLibs.getJam(eventNext.getEndDateTime());
        String MenitSelesai = DateLibs.getMinutes(eventNext.getEndDateTime());
        txtDay.setText(TanggalMulai+", ");
        txtHourandMinute.setText(JamMulai+":"+MenitMulai);
        txtMonth.setText(MonthMulai+" ");
        txtYear.setText(tanggal);
        btnPush.setEnabled(false);
        layoutTimer.setVisibility(View.GONE);
        String totalAtt = String.valueOf(eventNext.getAttendees().size());
        totalAttendee.setText("+"+totalAtt);
        btnDetailEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPressed(StatusListenerThisRoom,DataSend,ThisRoomAttendees,events,eventNext);
            }
        });
        btnPush.setText("Next Event");
        btnPush.setBackgroundResource(R.color.transparent);
        btnPush.setVisibility(View.GONE);
        return root;
    }
    private View LoadingStart(View root,LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        root = inflater.inflate(R.layout.layout_buffering, container, false);
        root.setBackgroundResource(R.color.Buffering_Violet);
        TextView textInfo = root.findViewById(R.id.infoBuffering);
        textInfo.setText("Please Wait, Check Meeting ... ");
        return root;
    }

    void Timer(TextView txtView, long totalWaktu){
        this.summaryText = txtView;

        new CountDownTimer(totalWaktu,1000){
            @Override
            public void onTick(long millisUntilFinished) {
                long millis= millisUntilFinished;
                String hms= String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
                );
                if(millis<240000){
                    String [] Datas = new String[6];
                    Datas[0] = "Warning";
                    Datas[1] = "Warning";
                    Datas[2] = "Warning";
                    Datas[3] = "Warning";
                    Datas[4] = "Warning";
                    Datas[5] = "Warning";
                    onPressed(StatusListenerThisRoom,Datas,ThisRoomCheckOut,events,eventNext);
                }
                summaryText.setText(hms);
            }

            @Override
            public void onFinish() {
                summaryText.setText("FINISH");
                String [] Datas = new String[6];
                Datas[0] = "Expired";
                Datas[1] = "Expired";
                Datas[2] = "Expired";
                Datas[3] = "Expired";
                Datas[4] = "Expired";
                Datas[5] = "Expired";
                onPressed(StatusListenerThisRoom,Datas,ThisRoomCheckOut,events,eventNext);
            }
        }.start();
    }

    public void onPressed(int StatusTypeListener, String[] Data,int ClickFor,Event events,EventNext eventNext) {
        if (mListener != null) {
            mListener.onFragmentInteraction(StatusTypeListener,Data,ClickFor, events,eventNext);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int StatusTypeListener, String[] data, int ClickFor, Event events, EventNext eventNext);
    }

    private void fn_countdown(long Timer) {
        if (Timer>0) {
            myProgress = 0;

            try {
                countDownTimer.cancel();

            } catch (Exception e) {

            }
            progress = 1;
            endTime = (int)Timer/1000; // up to finish time

            countDownTimer = new CountDownTimer(endTime*1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setProgress(progress, endTime);
                    progress = progress + 1;
                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                    int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
                    String newtime = hours + ":" + minutes + ":" + seconds;

                    if (newtime.equals("0:0:0")) {
                        tv_time.setText("00:00:00");
                    } else if ((String.valueOf(hours).length() == 1) && (String.valueOf(minutes).length() == 1) && (String.valueOf(seconds).length() == 1)) {
                        tv_time.setText("0" + hours + ":0" + minutes + ":0" + seconds);
                    } else if ((String.valueOf(hours).length() == 1) && (String.valueOf(minutes).length() == 1)) {
                        tv_time.setText("0" + hours + ":0" + minutes + ":" + seconds);
                    } else if ((String.valueOf(hours).length() == 1) && (String.valueOf(seconds).length() == 1)) {
                        tv_time.setText("0" + hours + ":" + minutes + ":0" + seconds);
                    } else if ((String.valueOf(minutes).length() == 1) && (String.valueOf(seconds).length() == 1)) {
                        tv_time.setText(hours + ":0" + minutes + ":0" + seconds);
                    } else if (String.valueOf(hours).length() == 1) {
                        tv_time.setText("0" + hours + ":" + minutes + ":" + seconds);
                    } else if (String.valueOf(minutes).length() == 1) {
                        tv_time.setText(hours + ":0" + minutes + ":" + seconds);
                    } else if (String.valueOf(seconds).length() == 1) {
                        tv_time.setText(hours + ":" + minutes + ":0" + seconds);
                    } else {
                        tv_time.setText(hours + ":" + minutes + ":" + seconds);
                    }

                }

                @Override
                public void onFinish() {
                    setProgress(progress, endTime);


                }
            };
            countDownTimer.start();
        }else {
          //  Toast.makeText(getActivity(),"Please enter the value",Toast.LENGTH_LONG).show();
        }

    }

    public void setProgress(int startTime, int endTime) {
        progressBarView.setMax(endTime);
        progressBarView.setSecondaryProgress(endTime);
        progressBarView.setProgress(startTime);

    }
}
