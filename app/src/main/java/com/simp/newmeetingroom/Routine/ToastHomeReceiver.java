package com.simp.newmeetingroom.Routine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;

public class ToastHomeReceiver extends BroadcastReceiver {
    int TypeService = 0;
    String Message;
    String[] Data = null;
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent= new Intent(context, ServiceHome.class);
        if (intent != null) {
            TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
            Data = intent.getStringArrayExtra(PAR_DATA_SERVICE);
            serviceIntent.putExtra(PAR_TYPE_SERVICE, TypeService);
            serviceIntent.putExtra(PAR_DATA_SERVICE, Data);
            context.startService(serviceIntent);
        } else {
            TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
            Data = intent.getStringArrayExtra(PAR_DATA_SERVICE);
            context.startService(serviceIntent);
        }
    }
}