package com.simp.newmeetingroom.Routine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.simp.newmeetingroom.ItemListHomeActivity;

public class AlarmBootingReceiver extends BroadcastReceiver {
    public static final String WAKE = "Wake up";
    @Override
    public void onReceive(Context context, Intent intent) {
        //Starting MainActivity
        Intent myAct = new Intent(context, ItemListHomeActivity.class);
        myAct.putExtra(WAKE, true);
        myAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(myAct);
        Log.i(WAKE,"BANGUN");
    }
}
