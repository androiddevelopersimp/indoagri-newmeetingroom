package com.simp.newmeetingroom.Routine;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_CANCELED;
import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.RESULT_CODE;
import static com.simp.newmeetingroom.Common.Constants.SendNotification;
import static com.simp.newmeetingroom.Common.Constants.ServiceBackToHome;
import static com.simp.newmeetingroom.Common.Constants.ServiceRoutine1Minute;

public class ServiceHome extends IntentService {
    public static final String ACTION="com.simp.newmeetingroom.Routine.ResponseHomeReceiver";
    int TypeService = 0;
    String[] Data = null;
    int resultCode = RESULT_CANCELED;
    public ServiceHome() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
        switch (TypeService){
            case ServiceRoutine1Minute:
                TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
                Data = intent.getStringArrayExtra(PAR_DATA_SERVICE);
                ServiceIdle(TypeService, Data);
                Log.i("SERVICE ROUTINE"," ACTIVE "+String.valueOf(TypeService));
                break;
            case ServiceBackToHome:
                TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
                Data = intent.getStringArrayExtra(PAR_DATA_SERVICE);
                ServiceBackToHome(TypeService, Data);
                Log.i("SERVICE BACK HOME"," ACTIVE "+String.valueOf(TypeService));
                break;
            case SendNotification:
                TypeService = intent.getIntExtra(PAR_TYPE_SERVICE,0);
                Data = intent.getStringArrayExtra(PAR_DATA_SERVICE);
                ServiceNotification(TypeService, Data);
                Log.i("NOTIFICATION"," ACTIVE "+String.valueOf(TypeService));
                break;
        }
    }

    void ServiceIdle(int typeService, String[] Data){
        Intent sendIntent= new Intent(ACTION);
        sendIntent.putExtra(RESULT_CODE, Activity.RESULT_OK);
        sendIntent.putExtra(PAR_TYPE_SERVICE,typeService);
        sendIntent.putExtra(PAR_DATA_SERVICE,Data);
        sendBroadcast(sendIntent);
    }
    void ServiceBackToHome(int typeService, String[] Data){
        Intent sendIntent= new Intent(ACTION);
        sendIntent.putExtra(RESULT_CODE, Activity.RESULT_OK);
        sendIntent.putExtra(PAR_TYPE_SERVICE,typeService);
        sendIntent.putExtra(PAR_DATA_SERVICE,Data);
        sendBroadcast(sendIntent);
    }
    void ServiceNotification(int typeService, String[] Data){
        Intent sendIntent= new Intent(ACTION);
        sendIntent.putExtra(RESULT_CODE, Activity.RESULT_OK);
        sendIntent.putExtra(PAR_TYPE_SERVICE,typeService);
        sendIntent.putExtra(PAR_DATA_SERVICE,Data);
        sendBroadcast(sendIntent);
    }
}