package com.simp.newmeetingroom.Routine;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.simp.newmeetingroom.Common.Constants;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.RESULT_CODE;

public class ResponseHomeReceiver extends BroadcastReceiver {

    BroadcastListener broadcastListener = null;

    public void IniBroadcastListener(BroadcastListener broadcastListener){

        this.broadcastListener = broadcastListener;
    }

    public ResponseHomeReceiver(){}

    @Override
    public void onReceive(Context context, Intent intent) {
        //get the broadcast message
        int RESULTCODE=intent.getIntExtra(RESULT_CODE,RESULT_CANCELED);
        int TYPESERVICE = intent.getIntExtra(PAR_TYPE_SERVICE,0);
        String[] DATA = intent.getStringArrayExtra(PAR_DATA_SERVICE);
        String resultMessage = intent.getStringExtra("ResultMessage");
        if (RESULTCODE==RESULT_OK){
            if(TYPESERVICE== Constants.SendNotification){
                if (broadcastListener!= null) {
                    broadcastListener.SetDataFromRefresh(RESULTCODE,TYPESERVICE,DATA);
                }
            }
            if (broadcastListener!= null) {
                broadcastListener.SetDataFromRefresh(RESULTCODE,TYPESERVICE,DATA);
            }
        }
    }

    public interface BroadcastListener{

        void SetDataFromRefresh(int resultCode,int resultTypeService,String[] Data);

    }

}
