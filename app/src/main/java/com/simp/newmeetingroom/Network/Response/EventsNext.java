package com.simp.newmeetingroom.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;

import java.util.List;

public class EventsNext {
    @SerializedName("event")
    @Expose
    private List<EventNext> eventNext = null;

    public List<EventNext> getEventNext() {
        return eventNext;
    }

    public void setEventNext(List<EventNext> eventNext) {
        this.eventNext = eventNext;
    }

}
