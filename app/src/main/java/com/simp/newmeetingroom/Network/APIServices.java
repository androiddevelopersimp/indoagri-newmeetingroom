package com.simp.newmeetingroom.Network;


import android.content.Context;

import com.simp.newmeetingroom.Apps;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Network.Response.Events;
import com.simp.newmeetingroom.Network.Response.EventsNext;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface  APIServices {
    Context context = Apps.getAppContext();
    @FormUrlEncoded
    @POST("auth/login")
    Call<com.simp.newmeetingroom.Model.Hardware> Login(@Field("HardSerial") String deviceid);

    @GET("event/feed")
    Call<Events> getEvents(@Query("room") String room,
                           @Query("day") int day,
                           @Query("curpage") int curpage,
                           @Query("pagesize") int pagesize);
    @GET("event/feed")
    Call<EventsNext> getEventsNext(@Query("room") String room,
                               @Query("day") int day,
                               @Query("curpage") int curpage,
                               @Query("pagesize") int pagesize);
    @GET("event/checkin")
    Call<Event> CheckIN(@Query("pin") String pin,
                        @Query("email") String email,
                        @Query("room") String room);
    @GET("event/checkout")
    Call<Event> CheckOUT(@Query("pin") String pin,
                             @Query("email") String email,
                             @Query("room") String room);

}
