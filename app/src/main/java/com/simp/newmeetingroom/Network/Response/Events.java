package com.simp.newmeetingroom.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;

import java.util.List;

public class Events {
    @SerializedName("event")
    @Expose
    private List<Event> event = null;

    public List<Event> getEvent() {
        return event;
    }

    public void setEvent(List<Event> event) {
        this.event = event;
    }

}
