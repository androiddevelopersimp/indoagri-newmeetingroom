package com.simp.newmeetingroom.Data;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.simp.newmeetingroom.Common.Constants;


public class SharePreference {
    Context _context;
    private SharedPreferences shared;
    private Editor editor;
    int PRIVATE_MODE = 0;
    private static String TAG = SharePreference.class.getSimpleName();
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_HARDKEY= "HARDKEY";
    private static final String KEY_ROOMID= "ROOMID";
    private static final String KEY_HARDSERIAL= "SERIAL";
    private static final String KEY_HARDBARCODE= "BARCODE";
    private static final String KEY_HARDNAME= "NAME";
    private static final String KEY_HARDPASSWORD= "PASSWORD";
    private static final String KEY_HARDMANUFACTURE= "MANUFACTURE";
    private static final String KEY_HARDBRAND= "BRAND";
    private static final String KEY_HARDMODEL= "MODEL";
    private static final String KEY_UPDATEDAT= "UPDATEDAT";
    private static final String KEY_ACTIVE= "ACTIVE";
    private static final String KEY_TIMEOFF= "TIMEOFF";
    private static final String KEY_TIMEON= "TIMEON";
    private static final String KEY_RINGTONE= "RINGTON";
    private static final String NEXT_EVENT = "NEXT_EVENT";


    public SharePreference(Context context){
        /*shared = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);*/
        this._context = context;
        shared = _context.getSharedPreferences(Constants.shared_name, PRIVATE_MODE);
        editor = shared.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }

    public boolean isLoggedIn(){
        return shared.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void setKeyActive(int isActive) {
        editor.putInt(KEY_ACTIVE, isActive);
        // commit changes
        editor.commit();
        Log.d(TAG, "GET ACTIVE HARDWARE");
    }
    public int isActive(){
        return shared.getInt(KEY_ACTIVE, 0);
    }

    public void setKeyHardkey(int hardkey) {
        editor.putInt(KEY_HARDKEY, hardkey);
        // commit changes
        editor.commit();
        Log.d(TAG, "ID NUMBER HARDWARE");
    }
    public String getKeyHardkey(){
        return shared.getString(KEY_HARDKEY, null);
    }

    public void setKeyRoomid(String roomid) {
        editor.putString(KEY_ROOMID, roomid);
        // commit changes
        editor.commit();
        Log.d(TAG, "ROOM ID HARDWARE");
    }
    public String getKeyRoomid(){
        return shared.getString(KEY_ROOMID, null);
    }

    public void setKeyHardserial(String hardserial) {
        editor.putString(KEY_HARDSERIAL, hardserial);
        // commit changes
        editor.commit();
        Log.d(TAG, "SERIAL NUMBER HARDWARE");
    }
    public String getKeyHardserial(){
        return shared.getString(KEY_HARDSERIAL, null);
    }

    public void setKeyHardbarcode(String hardbarcode) {
        editor.putString(KEY_HARDBARCODE, hardbarcode);
        // commit changes
        editor.commit();
        Log.d(TAG, "BARCODE HARDWARE");
    }
    public String getKeyHardbarcode(){
        return shared.getString(KEY_HARDBARCODE, null);
    }
    public void setKeyHardname(String hardname) {
        editor.putString(KEY_HARDNAME, hardname);
        // commit changes
        editor.commit();
        Log.d(TAG, "HARD NAME HARDWARE");
    }
    public String getKeyHardname(){
        return shared.getString(KEY_HARDNAME, null);
    }

    public void setKeyHardpassword(String hardpassword) {
        editor.putString(KEY_HARDPASSWORD, hardpassword);
        // commit changes
        editor.commit();
        Log.d(TAG, "PASSWORD HARDWARE");
    }
    public String getKeyHardpassword(){
        return shared.getString(KEY_HARDPASSWORD, null);
    }

    public void setKeyHardmanufacture(String hardmanufacture) {
        editor.putString(KEY_HARDMANUFACTURE, hardmanufacture);
        // commit changes
        editor.commit();
        Log.d(TAG, "MANUFACTURE HARDWARE");
    }
    public String getKeyHardmanufacture(){
        return shared.getString(KEY_HARDMANUFACTURE, null);
    }

    public void setKeyHardbrand(String hardbrand) {
        editor.putString(KEY_HARDBRAND, hardbrand);
        // commit changes
        editor.commit();
        Log.d(TAG, "BRAND HARDWARE");
    }
    public String getKeyHardbrand(){
        return shared.getString(KEY_HARDBRAND, null);
    }

    public void setKeyHardmodel(String hardmodel) {
        editor.putString(KEY_HARDMODEL, hardmodel);
        // commit changes
        editor.commit();
        Log.d(TAG, "MODEL HARDWARE");
    }
    public String getKeyHardmodel(){
        return shared.getString(KEY_HARDMODEL, null);
    }

    public void setKeyTimeoff(String timeoff) {
        editor.putString(KEY_TIMEOFF, timeoff);
        // commit changes
        editor.commit();
        Log.d(TAG, "TIME OFF HARDWARE");
    }
    public String getKeyTimeoff(){
        return shared.getString(KEY_TIMEOFF, null);
    }

    public void setKeyTimeon(String timeon) {
        editor.putString(KEY_TIMEON, timeon);
        // commit changes
        editor.commit();
        Log.d(TAG, "TIME ON HARDWARE");
    }
    public String getKeyTimeon(){
        return shared.getString(KEY_TIMEON, null);
    }


    public void setNextEvent(int nextEvent) {
        editor.putInt(NEXT_EVENT, nextEvent);
        // commit changes
        editor.commit();
        Log.d(TAG, "NEXT EVENT nUMBER");
    }
    public int getNextEvent(){
        return shared.getInt(NEXT_EVENT, 1);
    }










}