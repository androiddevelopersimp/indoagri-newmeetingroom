package com.simp.newmeetingroom;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.simp.newmeetingroom.Adapter.AdapterEvents;
import com.simp.newmeetingroom.Adapter.AdapterEventsNext;
import com.simp.newmeetingroom.Common.DateLibs;
import com.simp.newmeetingroom.Common.DeviceUtils;
import com.simp.newmeetingroom.Data.SharePreference;
import com.simp.newmeetingroom.Dialog.ProgressDialogs;
import com.simp.newmeetingroom.Dialog.ViewDialog;
import com.simp.newmeetingroom.Fragments.Dialog_DetailEvent_Fragment;
import com.simp.newmeetingroom.Fragments.Dialog_Push_Fragment;
import com.simp.newmeetingroom.Model.Attendee;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.Model.EventNext;
import com.simp.newmeetingroom.Network.APIServices;
import com.simp.newmeetingroom.Network.NetClient;
import com.simp.newmeetingroom.Network.Response.Events;
import com.simp.newmeetingroom.Network.Response.EventsNext;
import com.simp.newmeetingroom.Routine.ConnectivityReceiver;
import com.simp.newmeetingroom.Routine.ResponseHomeReceiver;
import com.simp.newmeetingroom.Routine.ServiceHome;
import com.simp.newmeetingroom.Routine.ToastHomeReceiver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.simp.newmeetingroom.Common.Constants.PAR_DATA_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.PAR_TYPE_SERVICE;
import static com.simp.newmeetingroom.Common.Constants.RESULT_CODE;
import static com.simp.newmeetingroom.Common.Constants.SendNotification;
import static com.simp.newmeetingroom.Common.Constants.ServiceBackToHome;

import static com.simp.newmeetingroom.Common.Constants.ServiceRoutine1Minute;
import static com.simp.newmeetingroom.Common.Constants.StatusListenerOtherRoom;
import static com.simp.newmeetingroom.Common.Constants.StatusListenerThisRoom;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomAttendees;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomCheckIn;
import static com.simp.newmeetingroom.Common.Constants.ThisRoomCheckOut;
import static com.simp.newmeetingroom.ItemDetailHomeFragment.SCOPE;


public class ItemListHomeActivity extends AppCompatActivity implements ItemDetailHomeFragment.OnFragmentInteractionListener, ResponseHomeReceiver.BroadcastListener, AdapterEvents.DataListAdapterCallback,
AdapterEventsNext.DataListAdapterCallbackNext, ConnectivityReceiver.ConnectivityReceiverListener,Dialog_Push_Fragment.DialogListener, Dialog_DetailEvent_Fragment.DialogDetailEventListener {
    private boolean mTwoPane;
    private static final String TAG = "HOME";

    Toolbar toolbar;
    AdapterEvents adapterEvents;
    FloatingActionButton fab;
    TextView txtJam, txtMenit, txtDetik;
    TextView txtDay, txtDate, txtMonth,txtYear;
    TextView txtRoomName;
    ResponseHomeReceiver responseHomeReceiver;
    ResponseHomeReceiver.BroadcastListener mBroadcastListener;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Intent intentSend;
    Event event;
    Events events;
    EventsNext eventNext;
    List<Event>eventList = new ArrayList<Event>();
    List<EventNext>eventNextList = new ArrayList<EventNext>();
    APIServices apiServices;
    public static TextView nowDate;
    public static TextView nextDate;
    ImageView wificonnect,wifilost;
    TextView txtipAddress;
    private boolean GetBack = true;
    boolean DialogOpens = false;
    boolean OpenEvent = false;
    boolean Vacant = false;
    Dialog dialog;
    ViewDialog alert;
    Dialog Pushdialog;
    ProgressDialogs Progressalert;
    boolean FlagFrom = false;
    TimeZone localTimeZone;
    ItemDetailHomeFragment itemDetailHomeFragment;
    boolean ringtone = false;
    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        apiServices = NetClient.AuthProduction().create(APIServices.class);
        setInitView();
        setInitPageDateTIme();
        BufferingRoom();
        checkConnection();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            boolean settingsCanWrite = Settings.System.canWrite(this);

            if(!settingsCanWrite) {
                Toast.makeText(this, "Require Permission to Handle Screen Brightness", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                startActivity(intent);
            }
        }
        localTimeZone = TimeZone.getDefault();
    }

    void setInitView(){
        new SharePreference(this).setNextEvent(1);
        dialog = new Dialog(ItemListHomeActivity.this);
        alert= new ViewDialog();
        alert.ViewDialog(dialog,ItemListHomeActivity.this,"can't communicate with the server");
        Pushdialog = new Dialog(ItemListHomeActivity.this);
        Progressalert= new ProgressDialogs();
        Progressalert.ProgressDialogs(Pushdialog,ItemListHomeActivity.this,"Please Wait");
        txtRoomName = (TextView)findViewById(R.id.room_name);
        String Strings = new SharePreference(ItemListHomeActivity.this).getKeyRoomid();
        String[] arrOfStrRoom = Strings.split("\\.");
        String RoomName = arrOfStrRoom[1];
        String[] arrOfStrRoom2 = RoomName.split("@");
        String NamaRuangan = null;
        if(arrOfStrRoom2.length<=15){
            NamaRuangan = arrOfStrRoom2[0]+" Room";
        }else{
            NamaRuangan = arrOfStrRoom2[0];
        }
        txtRoomName.setText(NamaRuangan);
        txtJam = (TextView) findViewById(R.id.jamId);
        AppCompatTextView VersionTxt = (findViewById(R.id.version));
        VersionTxt.setText("V. "+DeviceUtils.getAppVersion(ItemListHomeActivity.this));
        txtMenit = (TextView) findViewById(R.id.menitId);
        txtDetik = (TextView) findViewById(R.id.detikId);
        txtDay= (TextView) findViewById(R.id.days);
        txtDate = (TextView) findViewById(R.id.date);
        txtMonth = (TextView) findViewById(R.id.month);
        txtYear = (TextView) findViewById(R.id.year);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        wificonnect = (ImageView)findViewById(R.id.wificonnected);
        wifilost = (ImageView)findViewById(R.id.wifilost);
        txtipAddress = (TextView)findViewById(R.id.ipAddress);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        nowDate = (findViewById(R.id.desc_date));
        nextDate = (findViewById(R.id.next_date));

    }
    void setInitFunction(){
        setupServiceReceiver();
        String[] aa = new String[1];
        aa[0] = "ROUTINE";
        setAlarm(ServiceRoutine1Minute,aa,1);
       // doWorkNextDay();
    }
    void setInitPageDateTIme(){
        Thread myThread = null;
        Runnable runnable = new CountDownRunner();
        myThread= new Thread(runnable);
        myThread.start();

    }
    void BufferingRoom(){
        if (findViewById(R.id.item_detail_container) != null) {
            Fragment fragment;
            mTwoPane = true;
            Bundle arguments;
            String[] aa = new String[1];
            aa[0] = "BUFFERING";
            arguments = new Bundle();
            arguments.putInt(RESULT_CODE, 0 );
            arguments.putInt(PAR_TYPE_SERVICE, 0 );
            arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, null);
            arguments.putStringArray(PAR_DATA_SERVICE, aa);
            arguments.putString(SCOPE, "Today");
            fragment = new ItemDetailHomeFragment();
            fragment.setArguments(arguments);
            fragment.setArguments(arguments);
            this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();
        }

    }

    void setupServiceReceiver(){
        mBroadcastListener = this;
        responseHomeReceiver = new ResponseHomeReceiver();
        responseHomeReceiver.IniBroadcastListener(this);
        IntentFilter intentFilter= new IntentFilter();
        intentFilter.addAction(ServiceHome.ACTION);
        registerReceiver(responseHomeReceiver,intentFilter);
    }

    @Override
    public void onRowDataListAdapterClicked(int position, AppCompatActivity activity, boolean mTwoPane) {
      //  DummyContent.DummyItem item = DummyContent.ITEMS.get(position);
        Event eventData = eventList.get(position);
        if(eventData!=null){
            Bundle arguments;
            Fragment fragment;
            if (mTwoPane) {
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "BOOKED";
                //Toast.makeText(mParentActivity, String.valueOf(item.id), Toast.LENGTH_SHORT).show();
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putInt(PAR_TYPE_SERVICE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, eventData);
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
                OpenEvent = true;
            } else {
                Context context = activity;
                Intent intent = new Intent(context, ItemDetailHomeActivity.class);
                intent.putExtra(SCOPE,  "Today");
                intent.putExtra(ItemDetailHomeFragment.SENDDATA,  eventData);
                context.startActivity(intent);
            }
        }else{

        }

    }

    @Override
    public void onRowDataListAdapterClickedNext(int position, AppCompatActivity activity, boolean mTwoPane) {
        EventNext eventData = eventNextList.get(position);
        if(eventData!= null) {
            if (mTwoPane) {
                Bundle arguments;
                Fragment fragment;
                //Toast.makeText(mParentActivity, String.valueOf(item.id), Toast.LENGTH_SHORT).show();
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "BOOKED";
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putInt(PAR_TYPE_SERVICE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, eventData);
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Next");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
                OpenEvent = true;
            } else {
                Context context = activity;
                Intent intent = new Intent(context, ItemDetailHomeActivity.class);
                intent.putExtra(SCOPE, "Next");
                intent.putExtra(ItemDetailHomeFragment.SENDDATA, eventData);

                context.startActivity(intent);
            }
        }else{

        }
    }

    @Override
    public void onDialogPinNumber(String inputText, String[] Events) {
            if (TextUtils.isEmpty(inputText)) {
                DialogOpens = false;
            } else{
                DialogOpens = false;
            }
            if(!TextUtils.isEmpty(inputText)&& Events[5].equalsIgnoreCase("Checkin")){
                if (TextUtils.isEmpty(inputText)) {
                    DialogOpens = false;
                } else{
                    DialogOpens = false;
                    Progressalert.showDialog();
                    CheckIN(inputText,Events);
                    //Toast.makeText(this, inputText, Toast.LENGTH_SHORT).show();
                }
            }
            if(!TextUtils.isEmpty(inputText) && Events[5].equalsIgnoreCase("CheckOut")){
                if (TextUtils.isEmpty(inputText)) {
                    DialogOpens = false;
                } else{
                    DialogOpens = false;
                    Progressalert.showDialog();
                    CheckOUT(inputText,Events);
                }
            }
    }

    @Override
    public void onDialogEventDetail(String inputText, String[] DataEvents, Event eventDetail,EventNext eventNext) {
        if (TextUtils.isEmpty(inputText)) {
            DialogOpens = false;
        } else{
            DialogOpens = false;
        }
    }


    class CountDownRunner implements Runnable{
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try{
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeZone(localTimeZone);
                    Log.i("KEY:TIME",String.valueOf(localTimeZone));
                    Log.i("KEY-OPENEVENT",String.valueOf(OpenEvent));
                    Log.i("KEY-VACANT",String.valueOf(Vacant));
                    Log.i("KEY-DIALOGOPENS",String.valueOf(DialogOpens));
                    Log.i("KEY-RINGTONE",String.valueOf(ringtone));
               //     Calendar cal = Calendar.getInstance(Locale.getDefault());

                    int days = cal.get(Calendar.DAY_OF_WEEK);
                    int dates = cal.get(Calendar.DATE);
                    int months = cal.get(Calendar.MONTH);
                    int years = cal.get(Calendar.YEAR);

                    int hours = cal.get(Calendar.HOUR);
                    int minutes = cal.get(Calendar.MINUTE);
                    int seconds = cal.get(Calendar.SECOND);
                    int amPm = cal.get(Calendar.AM_PM);

                    switch (amPm) {
                        case Calendar.AM:

                            break;
                        case Calendar.PM:
                            hours = hours + 12;
                        default:
                            break;
                    }

                    txtJam.setText(digitFormat(hours,2)+":");
                    txtMenit.setText(digitFormat(minutes,2)+":");
                    txtDetik.setText(digitFormat(seconds,2));
                    String hari = "";

                    switch (days) {
                        case Calendar.SUNDAY:
                            hari = getResources().getString(R.string.minggu);
                            break;
                        case Calendar.MONDAY:
                            hari = getResources().getString(R.string.senin);
                            break;
                        case Calendar.TUESDAY:
                            hari = getResources().getString(R.string.selasa);
                            break;
                        case Calendar.WEDNESDAY:
                            hari = getResources().getString(R.string.rabu);
                            break;
                        case Calendar.THURSDAY:
                            hari = getResources().getString(R.string.kamis);
                            break;
                        case Calendar.FRIDAY:
                            hari = getResources().getString(R.string.jumat);
                            break;
                        case Calendar.SATURDAY:
                            hari = getResources().getString(R.string.sabtu);
                            break;
                        default:
                            break;
                    }


                    String bulan = "";

                    switch (months) {
                        case Calendar.JANUARY:
                            bulan = getResources().getString(R.string.januari);
                            break;
                        case Calendar.FEBRUARY:
                            bulan = getResources().getString(R.string.februari);
                            break;
                        case Calendar.MARCH:
                            bulan = getResources().getString(R.string.maret);
                            break;
                        case Calendar.APRIL:
                            bulan = getResources().getString(R.string.april);
                            break;
                        case Calendar.MAY:
                            bulan = getResources().getString(R.string.mei);
                            break;
                        case Calendar.JUNE:
                            bulan = getResources().getString(R.string.juni);
                            break;
                        case Calendar.JULY:
                            bulan = getResources().getString(R.string.juli);
                            break;
                        case Calendar.AUGUST:
                            bulan = getResources().getString(R.string.agustus);
                            break;
                        case Calendar.SEPTEMBER:
                            bulan = getResources().getString(R.string.september);
                            break;
                        case Calendar.OCTOBER:
                            bulan = getResources().getString(R.string.oktober);
                            break;
                        case Calendar.NOVEMBER:
                            bulan = getResources().getString(R.string.november);
                            break;
                        case Calendar.DECEMBER:
                            bulan = getResources().getString(R.string.desember);
                            break;
                        default:
                            break;
                    }
                    txtDay.setText(hari+", ");
                    txtDate.setText(bulan);
                    //txtMonth.setText(bulan+" ");
                    txtMonth.setVisibility(View.GONE);
                    if(dates<10){
                        txtYear.setText("0"+String.valueOf(dates));
                        nowDate.setText(bulan+" 0"+String.valueOf(dates));
                    }else{
                        txtYear.setText(String.valueOf(dates));
                        nowDate.setText(bulan+" "+String.valueOf(dates));
                    }

                }catch (Exception e) {}
            }
        });
    }

    public void doWorkNextDay() {
                    Calendar cal = Calendar.getInstance(Locale.getDefault());
                    cal.add(Calendar.DATE, 1);

                    int days = cal.get(Calendar.DAY_OF_WEEK);
                    int dates = cal.get(Calendar.DATE);
                    int months = cal.get(Calendar.MONTH);
                    String bulan = "";
                    switch (months) {
                        case Calendar.JANUARY:
                            bulan = getResources().getString(R.string.januari);
                            break;
                        case Calendar.FEBRUARY:
                            bulan = getResources().getString(R.string.februari);
                            break;
                        case Calendar.MARCH:
                            bulan = getResources().getString(R.string.maret);
                            break;
                        case Calendar.APRIL:
                            bulan = getResources().getString(R.string.april);
                            break;
                        case Calendar.MAY:
                            bulan = getResources().getString(R.string.mei);
                            break;
                        case Calendar.JUNE:
                            bulan = getResources().getString(R.string.juni);
                            break;
                        case Calendar.JULY:
                            bulan = getResources().getString(R.string.juli);
                            break;
                        case Calendar.AUGUST:
                            bulan = getResources().getString(R.string.agustus);
                            break;
                        case Calendar.SEPTEMBER:
                            bulan = getResources().getString(R.string.september);
                            break;
                        case Calendar.OCTOBER:
                            bulan = getResources().getString(R.string.oktober);
                            break;
                        case Calendar.NOVEMBER:
                            bulan = getResources().getString(R.string.november);
                            break;
                        case Calendar.DECEMBER:
                            bulan = getResources().getString(R.string.desember);
                            break;
                        default:
                            break;
                    }

                    nextDate.setText(String.valueOf(dates)+" "+bulan);

    }

    private String digitFormat(int value, int digit){
        String str = String.valueOf(value);

        int l = str.length();

        if(str.length() < digit){
            for(int i = l; i < digit; i++){
                str = "0" + str;
            }
        }

        return str;
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        cancelAlarm();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Apps.getInstance().setConnectivityListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    protected void onPause() {
        super.onPause();
       // unregisterReceiver(responseHomeReceiver);
        try {
            if (responseHomeReceiver!=null) {
                unregisterReceiver(responseHomeReceiver);
                responseHomeReceiver=null;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void SetDataFromRefresh(int resultCode, int resultTypeService,String[] data) {
        int TypeService = resultTypeService;
        Bundle arguments;
        Fragment fragment;
        String[] aa = new String[1];
        aa[0] = "ROUTINE";
        switch (TypeService) {
            case ServiceRoutine1Minute:
            case ServiceBackToHome:
                //WakeupWindows();
                String RoomParameter = new SharePreference(this).getKeyRoomid();
                int DayParameter = 0;
                int CurPageParameter = 0;
                int PageSizeParameter = 3;
                GetDataEvent(RoomParameter,DayParameter,CurPageParameter,PageSizeParameter);
                break;
            case SendNotification:
                WakeUPCuy();
                break;
            default:
                mTwoPane = true;
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0 );
                arguments.putInt(PAR_TYPE_SERVICE,0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, null);
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                fragment.setArguments(arguments);
                ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
        }
    }

    @Override
    public void onFragmentInteraction(int StatusTypeListener, String[] data, int ClickFor, Event dataEvents, EventNext dataEventNext) {
        int StatusList = StatusTypeListener;
        switch (StatusList){
            case StatusListenerThisRoom:
                DialogOpens = true;
                Dialog_Push_Fragment dialog_push_fragment = new Dialog_Push_Fragment();
                Dialog_DetailEvent_Fragment dialog_detailEvent_fragment = new Dialog_DetailEvent_Fragment();
                Bundle bundle;
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if(ClickFor == ThisRoomCheckIn){
                    dialog_push_fragment = new Dialog_Push_Fragment();
                    bundle = new Bundle();
                    bundle.putString("email", "xyz@gmail.com");
                    bundle.putBoolean("fullScreen", true);
                    bundle.putBoolean("notAlertDialog", true);
                    bundle.putStringArray("data",data);
                    dialog_push_fragment.setArguments(bundle);
                    ft = getSupportFragmentManager().beginTransaction();
                    prev = getSupportFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    dialog_push_fragment.show(ft, "dialog");
                }if(ClickFor == ThisRoomCheckOut){
                    if(data[5].equalsIgnoreCase("Expired")){
                        if(eventList.size()>0){
                            eventList.remove(0);
                            RecyclerView recyclerView = findViewById(R.id.item_list);
                            recyclerView.removeViewAt(0);
                            adapterEvents.notifyItemRemoved(0);
                            adapterEvents.notifyItemRangeChanged(0, eventList.size());
                            adapterEvents.notifyDataSetChanged();
                        }
                        DialogOpens = false;
                        Vacant = false;
                        BufferingRoom();
                    }
                if(data[5].equalsIgnoreCase("Checkout")){
                    bundle = new Bundle();
                    bundle.putString("email", "xyz@gmail.com");
                    bundle.putBoolean("fullScreen", true);
                    bundle.putBoolean("notAlertDialog", true);
                    bundle.putStringArray("data",data);
                    dialog_push_fragment.setArguments(bundle);
                    ft = getSupportFragmentManager().beginTransaction();
                    prev = getSupportFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    dialog_push_fragment.show(ft, "dialog");
                }
                if(data[5].equalsIgnoreCase("Warning")){
//                    ringtone = true;
//                    playingSound();
                }
              //  Toast.makeText(this, "CHECKOUT", Toast.LENGTH_SHORT).show();
            }if(ClickFor == ThisRoomAttendees){
                    if(dataEvents==null){
                        dialog_detailEvent_fragment = new Dialog_DetailEvent_Fragment();
                        bundle = new Bundle();
                        bundle.putBoolean(Dialog_DetailEvent_Fragment.EVENTTODAY,false);
                        bundle.putStringArray(Dialog_DetailEvent_Fragment.SENDDATASTRING,data);
                        bundle.putParcelable(Dialog_DetailEvent_Fragment.SENDDATADETAILEVENT, dataEventNext);
                        dialog_detailEvent_fragment.setArguments(bundle);
                        ft = getSupportFragmentManager().beginTransaction();
                        prev = getSupportFragmentManager().findFragmentByTag("dialog");
                        if (prev != null) {
                            ft.remove(prev);
                        }
                        ft.addToBackStack(null);
                        dialog_detailEvent_fragment.show(ft, "dialog");
                    }else{
                        dialog_detailEvent_fragment = new Dialog_DetailEvent_Fragment();
                        bundle = new Bundle();
                        bundle.putBoolean(Dialog_DetailEvent_Fragment.EVENTTODAY,true);
                        bundle.putStringArray(Dialog_DetailEvent_Fragment.SENDDATASTRING,data);
                        bundle.putParcelable(Dialog_DetailEvent_Fragment.SENDDATADETAILEVENT, dataEvents);
                        dialog_detailEvent_fragment.setArguments(bundle);
                        ft = getSupportFragmentManager().beginTransaction();
                        prev = getSupportFragmentManager().findFragmentByTag("dialog");
                        if (prev != null) {
                            ft.remove(prev);
                        }
                        ft.addToBackStack(null);
                        dialog_detailEvent_fragment.show(ft, "dialog");
                    }

//                for(int i=0; i<events.getAttendees().size(); i++){
//                    Attendee attendees = events.getAttendees().get(i);
//                    Toast.makeText(getApplicationContext(), "Berikut "+attendees.getEmail(), Toast.LENGTH_SHORT).show();
//                }
            }
                break;
            case StatusListenerOtherRoom:

                break;
                default:

        }
    }

    private void setAlarm(int RequestCode,String[] Data,int minutes) {
        int MILLISECOND_PER_MINUTE = 60 * 1000;
        int frequencyInMinutes = minutes;// 10 mins interval
        int TimeLess = frequencyInMinutes*MILLISECOND_PER_MINUTE;
        intentSend= new Intent(getApplicationContext(), ToastHomeReceiver.class);
        intentSend.putExtra(PAR_TYPE_SERVICE,RequestCode);
        intentSend.putExtra(PAR_DATA_SERVICE, Data);
         pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), RequestCode, intentSend,PendingIntent.FLAG_UPDATE_CURRENT);
        long startTime=System.currentTimeMillis(); //alarm starts immediately
        alarmManager=(AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,startTime,TimeLess,pendingIntent); // alarm will repeat after every 15 minutes
    }
    private void cancelAlarmParticular(int RequestCode) {
        intentSend= new Intent(getApplicationContext(), ToastHomeReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, RequestCode, intentSend, 0);
        alarmManager=(AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
    private void cancelAlarm(){
        alarmManager.cancel(pendingIntent);
    }

    // GET EVENT 1 //
    private void GetDataEvent(String room, int day, int curpage, int pagesize){
        //eventList.clear();
        getEvents(room,day,curpage,pagesize).enqueue(new Callback<Events>() {
            @Override
            public void onResponse(Call<Events> call, Response<Events> response) {
                // Got data. Send it to adapter
                Fragment fragment = null;
                Bundle arguments = null;
                if(response.code()==200){
                    if(response.body().getEvent().size()>0) {
                            events = eventResults(response);
                            eventList = events.getEvent();
                            CallData(eventList);
                            VacantRoom(fragment,arguments,eventList.get(0));
                        } else {
                        if(!DialogOpens){
                           AvailableRoom(fragment,arguments,null);
                        }else{
                            // tidak melakukan Apa apa//
                        }
                        }
                    if(GetBack){
                        CallNext();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Ada Kesalahan System", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Events> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ItemListHomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                // TODO: 08/11/16 handle failure
                //Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();

            }
        });
    }

    private Events eventResults(Response<Events> response) {
        Events fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<Events> getEvents(String room, int day, int curpage, int pagesize) {
        return apiServices.getEvents(room,day,curpage,pagesize);
    }

    private void CallData(List<Event>events){
        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        SHowData((RecyclerView) recyclerView,events);
    }
    private void SHowData(@NonNull RecyclerView recyclerView,List<Event> events){
        adapterEvents = new AdapterEvents(this, events,mTwoPane,this,nowDate);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterEvents);
        adapterEvents.notifyDataSetChanged();
    }

    // GET EVENT 2 //
    public void CallNext(){
        nextDate.setText("Wait...");
        String RoomParameter = new SharePreference(this).getKeyRoomid();
        int DayParameter = new SharePreference(this).getNextEvent();
        int CurPageParameter = 0;
        int PageSizeParameter = 1;
        GetDataEventNEXT(RoomParameter,DayParameter,CurPageParameter,PageSizeParameter);
    }
    private void GetDataEventNEXT(String room, int day, int curpage, int pagesize){
        //eventList.clear();
        getEventsNEXT(room,day,curpage,pagesize).enqueue(new Callback<EventsNext>() {
            @Override
            public void onResponse(Call<EventsNext> call, Response<EventsNext> response) {
                // Got data. Send it to adapter
                if(response.code()==200){
                    if(response.body().getEventNext().size()==0){
                        int DayParameter = new SharePreference(getApplicationContext()).getNextEvent();
                        if(DayParameter==1){
                            new SharePreference(getApplicationContext()).setNextEvent(2);
                            eventNext = eventResultsNEXT(response);
                            eventNextList= eventNext.getEventNext();
                            CallDataNEXT(eventNextList);
                            GetBack = true;
                        }if(DayParameter==2){
                            new SharePreference(getApplicationContext()).setNextEvent(1);
                            eventNext = eventResultsNEXT(response);
                            eventNextList= eventNext.getEventNext();
                            CallDataNEXT(eventNextList);
                            GetBack = true;
                        }
                    }else{
                        eventNext = eventResultsNEXT(response);
                        eventNextList= eventNext.getEventNext();
                        CallDataNEXT(eventNextList);
                        GetBack = true;
                    }
                }else{
                    GetBack = true;
                }
            }
            @Override
            public void onFailure(Call<EventsNext> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ItemListHomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                // TODO: 08/11/16 handle failure
           //     Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private EventsNext eventResultsNEXT(Response<EventsNext> response) {
        EventsNext fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<EventsNext> getEventsNEXT(String room, int day, int curpage, int pagesize) {
        return apiServices.getEventsNext(room,day,curpage,pagesize);
    }

    private void CallDataNEXT(List<EventNext>eventNextList){
        View recyclerView = findViewById(R.id.item_list_next);
        assert recyclerView != null;
        SHowDataNEXT((RecyclerView) recyclerView,eventNextList);
    }
    private void SHowDataNEXT(@NonNull RecyclerView recyclerView, List<EventNext>eventNexts){
        AdapterEventsNext adapterEventsNext = new AdapterEventsNext(this, eventNexts,mTwoPane,this,nextDate);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterEventsNext);
        adapterEventsNext.notifyDataSetChanged();
    }


    // CHECK IN //
    private void CheckIN(String pin, String[] Events){
        //eventList.clear();
        getPushEvents(pin,Events[2],Events[1]).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if(response.code()==200){
                    event = eventPushResults(response);
                    CallDataPush(event);
                }else{
                    Progressalert.dismissDialog();
                    dialogBoxWrongPIN();
                }
            }
            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ItemListHomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                // TODO: 08/11/16 handle failure
            }
        });
    }

    private Event eventPushResults(Response<Event> response) {
        Event eventPush = response.body();
        return eventPush;
    }

    private Call<Event> getPushEvents(String pin, String email, String room) {
        return apiServices.CheckIN(pin,email,room);
    }

    private void CallDataPush(Event events){
        Event event = events;
        Fragment fragment = null;
        Bundle arguments = null;
        InitialViewEventAfterCheckin(fragment,arguments,event);
    }

    // CHECK IN //
    private void CheckOUT(String pin, String[] Events){
        //eventList.clear();
        getPushEventsCheckOUT(pin,Events[2],Events[1]).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                // Got data. Send it to adapter
                if(response.code()==200){
                    Vacant = false;
                    event = eventPushResultsCheckOUT(response);
                    CallDataPushCheckOUT(event);
                }else{
                    Progressalert.dismissDialog();
                    dialogBoxWrongPIN();
                }
            }
            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ItemListHomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                // TODO: 08/11/16 handle failure
                //Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();

            }
        });
    }

    private Event eventPushResultsCheckOUT(Response<Event> response) {
        Event eventPush = response.body();
        return eventPush;
    }

    private Call<Event> getPushEventsCheckOUT(String pin, String email, String room) {
        return apiServices.CheckOUT(pin,email,room);
    }

    private void CallDataPushCheckOUT(Event events){
        Event event = events;
        Fragment fragment = null;
        Bundle arguments = null;
        Vacant = false;
        if(eventList.size()>0){
            eventList.remove(0);
            RecyclerView recyclerView = findViewById(R.id.item_list);
            recyclerView.removeViewAt(0);
            adapterEvents.notifyItemRemoved(0);
            adapterEvents.notifyItemRangeChanged(0, eventList.size());
            adapterEvents.notifyDataSetChanged();
        }
        String RoomParameter = new SharePreference(this).getKeyRoomid();
        int DayParameter = 0;
        int CurPageParameter = 0;
        int PageSizeParameter = 3;
        GetDataEvent(RoomParameter,DayParameter,CurPageParameter,PageSizeParameter);

    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        ConnectedWifi(isConnected);
    }
    private void ConnectedWifi(boolean isConnected){
        if(isConnected){
            wifilost.setVisibility(View.GONE);
            wificonnect.setVisibility(View.VISIBLE);
            alert.dismissDialog();
            setInitFunction();
            IntentFilter intentFilter= new IntentFilter();
            intentFilter.addAction(ServiceHome.ACTION);
            registerReceiver(responseHomeReceiver,intentFilter);
            WifiManager wifiMan = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            int ipAddress = wifiInf.getIpAddress();
            String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
            txtipAddress.setText(ip);

        }else{
            wifilost.setVisibility(View.VISIBLE);
            wificonnect.setVisibility(View.GONE);
            alert.showDialog();
            txtipAddress.setText("Not Connected");
            try {
                if (responseHomeReceiver!=null) {
                    unregisterReceiver(responseHomeReceiver);
                    responseHomeReceiver=null;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        ConnectedWifi(isConnected);
    }

    private void WakeUPCuy(){
        String[] aa = new String[1];
        aa[0] = "ROUTINE";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            this.setTurnScreenOn(true);
        } else {

            final Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
        Log.i(TAG,"WAKEUP");
        cancelAlarmParticular(SendNotification);
        setAlarm(ServiceRoutine1Minute,aa,1);
    }


    void InitialViewEvent(Fragment fragments,Bundle argument,Event event){
        Fragment fragment = fragments;
        Bundle arguments = argument;
        if (findViewById(R.id.item_detail_container) != null) {
            String Today = DateLibs.getToday();
            long WaktuNow = DateLibs.getDateTimeBy(Today);
            long WaktuEnding = DateLibs.getDateTimeByFromEvent(event.getEndDateTime());
            long WaktuMulai = DateLibs.getDateTimeByFromEvent(event.getStartDateTime());
            long BatasWaktuIdle = 900000;//15 Menit
            long TotalWaktuEvent = WaktuEnding - WaktuMulai;
            long SisaWaktuEvent = WaktuEnding - WaktuNow;
            if (WaktuNow < WaktuMulai) {
                Vacant = false;
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "AVAILABLE";
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, eventList.get(0));
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Vacant = true;
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "BOOKED";
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, eventList.get(0));
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            }
        }
    }
    void InitialViewEventAfterCheckin(Fragment fragments,Bundle argument,Event event){
        Progressalert.dismissDialog();
        Fragment fragment = fragments;
        Bundle arguments = argument;
        if (findViewById(R.id.item_detail_container) != null) {
            String Today = DateLibs.getToday();
            long WaktuNow = DateLibs.getDateTimeBy(Today);
            long WaktuEnding = DateLibs.getDateTimeByFromEvent(event.getEndDateTime());
            long WaktuMulai = DateLibs.getDateTimeByFromEvent(event.getStartDateTime());
            long BatasWaktuIdle = 900000;//15 Menit
            long TotalWaktuEvent = WaktuEnding - WaktuMulai;
            long SisaWaktuEvent = WaktuEnding - WaktuNow;
            if (WaktuNow < WaktuMulai) {
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "AVAILABLE";
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, event);
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                String[] aa = new String[2];
                aa[0] = "ROUTINE";
                aa[1] = "BOOKED";
                arguments = new Bundle();
                arguments.putInt(RESULT_CODE, 0);
                arguments.putParcelable(ItemDetailHomeFragment.SENDDATA,event);
                arguments.putStringArray(PAR_DATA_SERVICE, aa);
                arguments.putString(SCOPE, "Today");
                fragment = new ItemDetailHomeFragment();
                fragment.setArguments(arguments);
                ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            }
        }
        String RoomParameter = new SharePreference(this).getKeyRoomid();
        int DayParameter = 0;
        int CurPageParameter = 0;
        int PageSizeParameter = 3;
        GetDataEvent(RoomParameter,DayParameter,CurPageParameter,PageSizeParameter);
    }

    public void dialogBoxWrongPIN() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("INCORRECT YOUR PIN");
        alertDialogBuilder.setPositiveButton("RETRY",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    void VacantRoom(Fragment fragment,Bundle arguments,Event eventObject){
        Progressalert.dismissDialog();
        String Status = eventObject.getEventStatus();
        if(Status.equalsIgnoreCase("2")){
            if(!DialogOpens && !OpenEvent && !Vacant){
                InitialViewEvent(fragment,arguments,eventObject);
            }if(!DialogOpens && OpenEvent && !Vacant){
                InitialViewEvent(fragment,arguments,eventObject);
                Vacant = true;
                OpenEvent = false;
            }if(!DialogOpens && OpenEvent && Vacant){
                InitialViewEvent(fragment,arguments,eventObject);
                Vacant = true;
                OpenEvent = false;
            }
            else{

            }
        }else{
            if(!DialogOpens){
                InitialViewEvent(fragment,arguments,eventList.get(0));
            }else{

            }
        }
    }
    void AvailableRoom(Fragment fragment,Bundle arguments,Event eventObject){
        Progressalert.dismissDialog();
        arguments = new Bundle();
        String[] aa = new String[2];
        aa[0] = "ROUTINE";
        aa[1] = "AVAILABLE";
        arguments.putInt(RESULT_CODE, 0);
        arguments.putInt(PAR_TYPE_SERVICE, 0);
        arguments.putParcelable(ItemDetailHomeFragment.SENDDATA, null);
        arguments.putStringArray(PAR_DATA_SERVICE, aa);
        arguments.putString(SCOPE, "Today");
        fragment = new ItemDetailHomeFragment();
        fragment.setArguments(arguments);
        ItemListHomeActivity.this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();
        Vacant = false;
    }

}
