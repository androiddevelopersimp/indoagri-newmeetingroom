package com.simp.newmeetingroom.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventNext implements Parcelable {
    public static final String TABLE_NAME = "EVENTS";
    @SerializedName("eventId")
    @Expose
    private Integer eventId;
    @SerializedName("eventUID")
    @Expose
    private String eventUID;
    @SerializedName("roomEmail")
    @Expose
    private String roomEmail;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("organizer")
    @Expose
    private String organizer;
    @SerializedName("organizerDomain")
    @Expose
    private Object organizerDomain;
    @SerializedName("organizerFullName")
    @Expose
    private String organizerFullName;
    @SerializedName("organizerEmail")
    @Expose
    private String organizerEmail;
    @SerializedName("eventStatus")
    @Expose
    private String eventStatus;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("startDateTime")
    @Expose
    private String startDateTime;
    @SerializedName("endDateTime")
    @Expose
    private String endDateTime;
    @SerializedName("endDateActual")
    @Expose
    private String endDateActual;
    @SerializedName("startDateActual")
    @Expose
    private String startDateActual;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("ext")
    @Expose
    private Object ext;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("isDeleted")
    @Expose
    private Object isDeleted;
    @SerializedName("idle")
    @Expose
    private Object idle;
    @SerializedName("checkInFlag")
    @Expose
    private Object checkInFlag;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;

    protected EventNext(Parcel in) {
        if (in.readByte() == 0) {
            eventId = null;
        } else {
            eventId = in.readInt();
        }
        eventUID = in.readString();
        roomEmail = in.readString();
        summary = in.readString();
        organizer = in.readString();
        organizerFullName = in.readString();
        organizerEmail = in.readString();
        eventStatus = in.readString();
        description = in.readString();
        startDateTime = in.readString();
        endDateTime = in.readString();
        endDateActual = in.readString();
        startDateActual = in.readString();
        pin = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<EventNext> CREATOR = new Creator<EventNext>() {
        @Override
        public EventNext createFromParcel(Parcel in) {
            return new EventNext(in);
        }

        @Override
        public EventNext[] newArray(int size) {
            return new EventNext[size];
        }
    };

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventUID() {
        return eventUID;
    }

    public void setEventUID(String eventUID) {
        this.eventUID = eventUID;
    }

    public String getRoomEmail() {
        return roomEmail;
    }

    public void setRoomEmail(String roomEmail) {
        this.roomEmail = roomEmail;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Object getOrganizerDomain() {
        return organizerDomain;
    }

    public void setOrganizerDomain(Object organizerDomain) {
        this.organizerDomain = organizerDomain;
    }

    public String getOrganizerFullName() {
        return organizerFullName;
    }

    public void setOrganizerFullName(String organizerFullName) {
        this.organizerFullName = organizerFullName;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getEndDateActual() {
        return endDateActual;
    }

    public void setEndDateActual(String endDateActual) {
        this.endDateActual = endDateActual;
    }

    public String getStartDateActual() {
        return startDateActual;
    }

    public void setStartDateActual(String startDateActual) {
        this.startDateActual = startDateActual;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Object getIdle() {
        return idle;
    }

    public void setIdle(Object idle) {
        this.idle = idle;
    }

    public Object getCheckInFlag() {
        return checkInFlag;
    }

    public void setCheckInFlag(Object checkInFlag) {
        this.checkInFlag = checkInFlag;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (eventId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(eventId);
        }
        parcel.writeString(eventUID);
        parcel.writeString(roomEmail);
        parcel.writeString(summary);
        parcel.writeString(organizer);
        parcel.writeString(organizerFullName);
        parcel.writeString(organizerEmail);
        parcel.writeString(eventStatus);
        parcel.writeString(description);
        parcel.writeString(startDateTime);
        parcel.writeString(endDateTime);
        parcel.writeString(endDateActual);
        parcel.writeString(startDateActual);
        parcel.writeString(pin);
        parcel.writeString(updatedAt);
    }
}
