package com.simp.newmeetingroom.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendee {

    @SerializedName("attendeeId")
    @Expose
    private Integer attendeeId;
    @SerializedName("eventId")
    @Expose
    private Integer eventId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("attendeeType")
    @Expose
    private String attendeeType;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("isPresent")
    @Expose
    private Object isPresent;
    @SerializedName("modetator")
    @Expose
    private Object modetator;

    public Integer getAttendeeId() {
        return attendeeId;
    }

    public void setAttendeeId(Integer attendeeId) {
        this.attendeeId = attendeeId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAttendeeType() {
        return attendeeType;
    }

    public void setAttendeeType(String attendeeType) {
        this.attendeeType = attendeeType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(Object isPresent) {
        this.isPresent = isPresent;
    }

    public Object getModetator() {
        return modetator;
    }

    public void setModetator(Object modetator) {
        this.modetator = modetator;
    }

}