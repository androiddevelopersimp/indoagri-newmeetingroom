package com.simp.newmeetingroom.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hardware implements Parcelable {

    @SerializedName("hardKey")
    @Expose
    private Integer hardKey;
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("hardSerial")
    @Expose
    private String hardSerial;
    @SerializedName("hardBarcode")
    @Expose
    private String hardBarcode;
    @SerializedName("hardName")
    @Expose
    private String hardName;
    @SerializedName("hardPassword")
    @Expose
    private String hardPassword;
    @SerializedName("hardManufacture")
    @Expose
    private String hardManufacture;
    @SerializedName("hardBrand")
    @Expose
    private String hardBrand;
    @SerializedName("hardModel")
    @Expose
    private String hardModel;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("timeOff")
    @Expose
    private String timeOff;
    @SerializedName("timeOn")
    @Expose
    private String timeOn;
    @SerializedName("ringtone")
    @Expose
    private String ringtone;

    public Integer getHardKey() {
        return hardKey;
    }

    public void setHardKey(Integer hardKey) {
        this.hardKey = hardKey;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getHardSerial() {
        return hardSerial;
    }

    public void setHardSerial(String hardSerial) {
        this.hardSerial = hardSerial;
    }

    public String getHardBarcode() {
        return hardBarcode;
    }

    public void setHardBarcode(String hardBarcode) {
        this.hardBarcode = hardBarcode;
    }

    public String getHardName() {
        return hardName;
    }

    public void setHardName(String hardName) {
        this.hardName = hardName;
    }

    public String getHardPassword() {
        return hardPassword;
    }

    public void setHardPassword(String hardPassword) {
        this.hardPassword = hardPassword;
    }

    public String getHardManufacture() {
        return hardManufacture;
    }

    public void setHardManufacture(String hardManufacture) {
        this.hardManufacture = hardManufacture;
    }

    public String getHardBrand() {
        return hardBrand;
    }

    public void setHardBrand(String hardBrand) {
        this.hardBrand = hardBrand;
    }

    public String getHardModel() {
        return hardModel;
    }

    public void setHardModel(String hardModel) {
        this.hardModel = hardModel;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(String timeOff) {
        this.timeOff = timeOff;
    }

    public String getTimeOn() {
        return timeOn;
    }

    public void setTimeOn(String timeOn) {
        this.timeOn = timeOn;
    }

    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.hardKey);
        dest.writeString(this.roomId);
        dest.writeString(this.hardSerial);
        dest.writeString(this.hardBarcode);
        dest.writeString(this.hardName);
        dest.writeString(this.hardPassword);
        dest.writeString(this.hardManufacture);
        dest.writeString(this.hardBrand);
        dest.writeString(this.hardModel);
        dest.writeString(this.updatedAt);
        dest.writeValue(this.active);
        dest.writeString(this.timeOff);
        dest.writeString(this.timeOn);
        dest.writeString(this.ringtone);
    }

    public Hardware() {
    }

    protected Hardware(Parcel in) {
        this.hardKey = (Integer) in.readValue(Integer.class.getClassLoader());
        this.roomId = in.readString();
        this.hardSerial = in.readString();
        this.hardBarcode = in.readString();
        this.hardName = in.readString();
        this.hardPassword = in.readString();
        this.hardManufacture = in.readString();
        this.hardBrand = in.readString();
        this.hardModel = in.readString();
        this.updatedAt = in.readString();
        this.active = (Integer) in.readValue(Integer.class.getClassLoader());
        this.timeOff = in.readString();
        this.timeOn = in.readString();
        this.ringtone = in.readString();
    }

    public static final Parcelable.Creator<Hardware> CREATOR = new Parcelable.Creator<Hardware>() {
        @Override
        public Hardware createFromParcel(Parcel source) {
            return new Hardware(source);
        }

        @Override
        public Hardware[] newArray(int size) {
            return new Hardware[size];
        }
    };
}