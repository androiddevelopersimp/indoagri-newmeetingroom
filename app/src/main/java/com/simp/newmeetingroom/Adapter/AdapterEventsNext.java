package com.simp.newmeetingroom.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.simp.newmeetingroom.Common.DateLocal;
import com.simp.newmeetingroom.Model.EventNext;
import com.simp.newmeetingroom.R;

import java.util.Calendar;
import java.util.List;

public class AdapterEventsNext extends RecyclerView.Adapter<AdapterEventsNext.ViewHolder> {
    private final AppCompatActivity mParentActivity;
    private final List<EventNext> eventList;
    private final boolean mTwoPane;
    private final AdapterEventsNext.DataListAdapterCallbackNext mOnClickListener;
    TextView NextDayTitle;
    public AdapterEventsNext(AppCompatActivity parent,
                             List<EventNext> items,
                             boolean twoPane, AdapterEventsNext.DataListAdapterCallbackNext mOnClickListener, TextView NextDayTitle) {
        eventList = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
        this.NextDayTitle = NextDayTitle;
        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public AdapterEventsNext.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_event, parent, false);
        return new AdapterEventsNext.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEventsNext.ViewHolder holder, int position) {
        DateLocal dateLocal = new DateLocal();
        String month = eventList.get(position).getStartDateTime().substring(5,7);
        String date = eventList.get(position).getStartDateTime().substring(8,10);
        String NamaBulan  = dateLocal.getMonthAlphabet(month);
        String Summary = eventList.get(position).getSummary();
        String SummaryData = null;
        if(Summary != null){
            if (eventList.get(position).getSummary().length() >= 35) {
                SummaryData = eventList.get(position).getSummary().substring(0, 35)+ "...";
            } else {
                SummaryData = eventList.get(position).getSummary();
            }
        }else{
            SummaryData = "Event "+eventList.get(position).getOrganizer();
        }
        holder.startEventClock.setText(eventList.get(position).getStartDateTime().substring(11,16));
        holder.endEventClock.setText(eventList.get(position).getEndDateTime().substring(11,16));
        holder.organizer.setText("Organizer : "+eventList.get(position).getOrganizerFullName());
        holder.description.setText(SummaryData);
        holder.itemView.setTag(eventList.get(position));
        if(position==eventList.size()-1){
            holder.viewLine.setVisibility(View.GONE);
        }
        NextDayTitle.setText(NamaBulan+" "+date);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView startEventClock;
        final TextView endEventClock;
        final TextView organizer;
        final TextView description;
        final View viewLine;

        ViewHolder(View view) {
            super(view);
            startEventClock = (TextView) view.findViewById(R.id.startEventClock);
            endEventClock = (TextView) view.findViewById(R.id.endEventClock);
            organizer = (TextView)view.findViewById(R.id.organizerEvent);
            description = (TextView)view.findViewById(R.id.descriptionEvent);
            viewLine = (View)view.findViewById(R.id.line);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    Memanggil interface dan juga methodnya. getAdapterPosition ini adalah method bawaan
                    adapter untuk memanggil index posisi.
                     */
                    mOnClickListener.onRowDataListAdapterClickedNext(getAdapterPosition(),mParentActivity,mTwoPane);
                }
            });
        }
    }

    public interface DataListAdapterCallbackNext {
        void onRowDataListAdapterClickedNext(int position, AppCompatActivity activity, boolean mTwoPane);
    }
    

}
