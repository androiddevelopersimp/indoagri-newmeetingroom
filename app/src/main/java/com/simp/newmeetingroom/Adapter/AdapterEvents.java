package com.simp.newmeetingroom.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.simp.newmeetingroom.Common.DateLibs;
import com.simp.newmeetingroom.Common.DateLocal;
import com.simp.newmeetingroom.Model.Event;
import com.simp.newmeetingroom.R;
import com.simp.newmeetingroom.dummy.DummyContent;

import java.util.List;

public class AdapterEvents extends RecyclerView.Adapter<AdapterEvents.ViewHolder> {
    private final AppCompatActivity mParentActivity;
    private final List<Event> eventList;
    private final boolean mTwoPane;
    private final AdapterEvents.DataListAdapterCallback mOnClickListener;
    TextView nowDate;
    public AdapterEvents(AppCompatActivity parent,
                        List<Event> items,
                        boolean twoPane, AdapterEvents.DataListAdapterCallback mOnClickListener,
                         TextView nowDate) {
        eventList = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
        this.nowDate  = nowDate;
        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public AdapterEvents.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_event, parent, false);
        return new AdapterEvents.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEvents.ViewHolder holder, int position) {
        DateLocal dateLocal = new DateLocal();
        String month = eventList.get(position).getStartDateTime().substring(5,7);
        String date = eventList.get(position).getStartDateTime().substring(8,10);
        String statusEvent  = eventList.get(position).getEventStatus();
        String NamaBulan  = dateLocal.getMonthAlphabet(month);
        String Summary = eventList.get(position).getSummary();
        String SummaryData = null;
        if(Summary != null){
            if (eventList.get(position).getSummary().length() >= 35) {
                SummaryData = eventList.get(position).getSummary().substring(0, 35)+ "...";
            } else {
                SummaryData = eventList.get(position).getSummary();
            }
        }else{
            SummaryData = "Event "+eventList.get(position).getOrganizer();
        }
        holder.startEventClock.setText(eventList.get(position).getStartDateTime().substring(11,16));
        holder.endEventClock.setText(eventList.get(position).getEndDateTime().substring(11,16));
        holder.organizer.setText("Organizer : "+eventList.get(position).getOrganizerFullName());
        holder.description.setText(SummaryData);
        if(position==eventList.size()-1){
            holder.viewLine.setVisibility(View.GONE);
        }
        if(statusEvent.contains("2")){
            String Today = DateLibs.getToday();
            long WaktuNow = DateLibs.getDateTimeBy(Today);
            long WaktuEnding = DateLibs.getDateTimeByFromEvent(eventList.get(position).getEndDateTime());
            long WaktuMulai = DateLibs.getDateTimeByFromEvent(eventList.get(position).getStartDateTime());
            if(WaktuNow<WaktuMulai){
                holder.iconVacant.setVisibility(View.GONE);
                holder.iconBooked.setVisibility(View.VISIBLE);
            }else{
                holder.iconVacant.setVisibility(View.VISIBLE);
                holder.iconBooked.setVisibility(View.GONE);
            }
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mParentActivity,R.color.Vacant_Red));
//            holder.layout.setBackgroundResource(R.color.Vacant_Red);
        }
        if(statusEvent.contains("3")){
           holder.itemView.setVisibility(View.GONE);
        }else{
            holder.itemView.setBackgroundResource(R.color.colorBase);
        }
//        if (position % 2 == 0) {
//            holder.itemView.setBackgroundResource(R.color.colorAbu2);
//        } else {
//            holder.itemView.setBackgroundResource(R.color.colorBase);
//        }
        nowDate.setText(date+" "+NamaBulan);
        holder.itemView.setTag(eventList.get(position));
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView startEventClock;
        final TextView endEventClock;
        final TextView organizer;
        final TextView description;
        final View viewLine;
        final LinearLayout layout;
        final ImageView iconVacant,iconBooked;

        ViewHolder(View view) {
            super(view);
            startEventClock = (TextView) view.findViewById(R.id.startEventClock);
            endEventClock = (TextView) view.findViewById(R.id.endEventClock);
            organizer = (TextView)view.findViewById(R.id.organizerEvent);
            description = (TextView)view.findViewById(R.id.descriptionEvent);
            iconBooked = (ImageView)view.findViewById(R.id.iconBooked);
            iconVacant = (ImageView)view.findViewById(R.id.iconVacant);
            layout = (LinearLayout) view.findViewById(R.id.LayoutItem);

            viewLine = (View)view.findViewById(R.id.line);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    Memanggil interface dan juga methodnya. getAdapterPosition ini adalah method bawaan
                    adapter untuk memanggil index posisi.
                     */
                    mOnClickListener.onRowDataListAdapterClicked(getAdapterPosition(),mParentActivity,mTwoPane);
                }
            });
        }
    }

    public interface DataListAdapterCallback {
        void onRowDataListAdapterClicked(int position,AppCompatActivity activity,boolean mTwoPane);
    }

}
