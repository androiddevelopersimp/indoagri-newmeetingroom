package com.simp.newmeetingroom.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.simp.newmeetingroom.Fragments.Dialog_DetailEvent_Fragment;
import com.simp.newmeetingroom.Model.Attendee;
import com.simp.newmeetingroom.R;

import java.util.List;

public class AdapterAttendees extends RecyclerView.Adapter<AdapterAttendees.ViewHolder> {
    private final Dialog_DetailEvent_Fragment mParentActivity;
    private final List<Attendee> attendeeList;
    public AdapterAttendees(Dialog_DetailEvent_Fragment parent,
                            List<Attendee> items) {
        attendeeList = items;
        mParentActivity = parent;
    }

    @NonNull
    @Override
    public AdapterAttendees.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_attendee, parent, false);
        return new AdapterAttendees.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterAttendees.ViewHolder holder, int position) {
        String atteEmail = attendeeList.get(position).getEmail();
        holder.attendeeTxt.setText(atteEmail);
        holder.itemView.setTag(attendeeList.get(position));
    }

    @Override
    public int getItemCount() {
        return attendeeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView attendeeTxt;
        final View viewLine;

        ViewHolder(View view) {
            super(view);
            attendeeTxt = (TextView) view.findViewById(R.id.attendee);
            viewLine = (View)view.findViewById(R.id.line);
//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    /*
//                    Memanggil interface dan juga methodnya. getAdapterPosition ini adalah method bawaan
//                    adapter untuk memanggil index posisi.
//                     */
//                    mOnClickListener.onRowDataListAdapterClicked(getAdapterPosition(),mParentActivity,mTwoPane);
//                }
//            });
        }
    }

}
