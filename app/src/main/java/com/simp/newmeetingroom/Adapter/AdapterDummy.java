package com.simp.newmeetingroom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.simp.newmeetingroom.ItemListHomeActivity;
import com.simp.newmeetingroom.R;
import com.simp.newmeetingroom.dummy.DummyContent;

import java.util.List;

public class AdapterDummy extends RecyclerView.Adapter<AdapterDummy.ViewHolder> {

    private final AppCompatActivity mParentActivity;
    private final List<DummyContent.DummyItem> mValues;
    private final boolean mTwoPane;
    private final DataListAdapterCallback mOnClickListener;
    public AdapterDummy(AppCompatActivity parent,
                        List<DummyContent.DummyItem> items,
                        boolean twoPane, DataListAdapterCallback mOnClickListener) {
        mValues = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);
        holder.itemView.setTag(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    Memanggil interface dan juga methodnya. getAdapterPosition ini adalah method bawaan
                    adapter untuk memanggil index posisi.
                     */
                    mOnClickListener.onRowDataListAdapterClicked(getAdapterPosition(),mParentActivity,mTwoPane);
                }
            });
        }
    }

    public interface DataListAdapterCallback {
        void onRowDataListAdapterClicked(int position,AppCompatActivity activity,boolean mTwoPane);
    }
}
