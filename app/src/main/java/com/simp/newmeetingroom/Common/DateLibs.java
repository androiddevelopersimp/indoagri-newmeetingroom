package com.simp.newmeetingroom.Common;



import android.net.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/***
 * Provides helper methods for date utilities.
 * Yes, some times I write shit code.
 */
public class DateLibs {

    public static String getDay(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getDayTanggal(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getDay2(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getHariini() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String getTanggalandTime(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }



    public static String getTgl(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }

    public static String getJam(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getMinutes(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("mm");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getMonth2(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getYear(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yy");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String ConvertDateToDateTime(String dateString)
    {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date convertedDate = null;
        try {
            try {
                convertedDate = dateFormat.parse(dateString);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String startdate = dateFormat.format(convertedDate);
        return startdate;
    }
    public static String getToday() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String getWaktuIdle(String StartDateTimes){
        Date date1 = null;
        String IdlesTime = null;
        TimeZone  localTimeZone  = TimeZone.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);
        try {
            date1 = dateFormat.parse(StartDateTimes);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            cal1.add(Calendar.MINUTE, +15);
            IdlesTime = dateFormat.format(cal1.getTime());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return IdlesTime;
    }
    public static String getTodayDate() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String nextTomorrow() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String nextWeek() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.WEEK_OF_MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String nextMonth() {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String beforeMonth(int jumlah) {
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(localTimeZone);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.MONTH, -jumlah);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String getUtcTime(String dateAndTime) {
        Date d = parseDate(dateAndTime);

        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());

        // Convert Local Time to UTC
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(d);
    }

    /****
     * Parses date string and return a {@link Date} object
     *
     * @return The ISO formatted date object
     */
    public static Date parseDate(String date) {

        if (date == null) {
            return null;
        }

        StringBuffer sbDate = new StringBuffer();
        sbDate.append(date);
        String newDate = null;
        Date dateDT = null;

        try {
            newDate = sbDate.substring(0, 19).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String rDate = newDate.replace("T", " ");
        String nDate = rDate.replaceAll("-", "/");

        try {
            dateDT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault()).parse(nDate);
            // Log.v( TAG, "#parseDate dateDT: " + dateDT );
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateDT;
    }

    public static Date toLocalTime(String utcDate, SimpleDateFormat sdf) throws Exception {

        // create a new Date object using
        // the timezone of the specified city
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date localDate = sdf.parse(utcDate);

        sdf.setTimeZone(TimeZone.getDefault());
        String dateFormateInUTC = sdf.format(localDate);

        return sdf.parse(dateFormateInUTC);
    }

    /**
     * Returns abbreviated (3 letters) day of the week.
     *
     * @param date ISO format date
     * @return The name of the day of the week
     */
    public static String getDayOfWeekAbbreviated(String date) {
        Date dateDT = parseDate(date);
        TimeZone  localTimeZone  = TimeZone.getDefault();
        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        c.setTimeZone(localTimeZone);
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.DAY_OF_WEEK);


        String dayStr = null;


        switch (day) {

            case Calendar.SUNDAY:
                dayStr = "Sunday";
                break;

            case Calendar.MONDAY:
                dayStr = "Monday";
                break;

            case Calendar.TUESDAY:
                dayStr = "Tuesday";
                break;

            case Calendar.WEDNESDAY:
                dayStr = "Wednesday";
                break;

            case Calendar.THURSDAY:
                dayStr = "Thursday";
                break;

            case Calendar.FRIDAY:
                dayStr = "Friday";
                break;

            case Calendar.SATURDAY:
                dayStr = "Saturday";
                break;
        }

        return dayStr;
    }

    /***
     * Gets the name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     */
    public static String getMonth(String date) {
        Date dateDT = parseDate(date);
        TimeZone  localTimeZone  = TimeZone.getDefault();
        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        c.setTimeZone(localTimeZone);
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.MONTH);

        String dayStr = null;

        switch (day) {

            case Calendar.JANUARY:
                dayStr = "January";
                break;

            case Calendar.FEBRUARY:
                dayStr = "February";
                break;

            case Calendar.MARCH:
                dayStr = "March";
                break;

            case Calendar.APRIL:
                dayStr = "April";
                break;

            case Calendar.MAY:
                dayStr = "May";
                break;

            case Calendar.JUNE:
                dayStr = "June";
                break;

            case Calendar.JULY:
                dayStr = "July";
                break;

            case Calendar.AUGUST:
                dayStr = "August";
                break;

            case Calendar.SEPTEMBER:
                dayStr = "September";
                break;

            case Calendar.OCTOBER:
                dayStr = "October";
                break;

            case Calendar.NOVEMBER:
                dayStr = "November";
                break;

            case Calendar.DECEMBER:
                dayStr = "December";
                break;
        }

        return dayStr;
    }

    /**
     * Gets abbreviated name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     */
    public static String getMonthAbbreviated(String date) {
        Date dateDT = parseDate(date);
        TimeZone  localTimeZone  = TimeZone.getDefault();
        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        c.setTimeZone(localTimeZone);
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.MONTH);

        String dayStr = null;

        switch (day) {

            case Calendar.JANUARY:
                dayStr = "Jan";
                break;

            case Calendar.FEBRUARY:
                dayStr = "Feb";
                break;

            case Calendar.MARCH:
                dayStr = "Mar";
                break;

            case Calendar.APRIL:
                dayStr = "Apr";
                break;

            case Calendar.MAY:
                dayStr = "May";
                break;

            case Calendar.JUNE:
                dayStr = "Jun";
                break;

            case Calendar.JULY:
                dayStr = "Jul";
                break;

            case Calendar.AUGUST:
                dayStr = "Aug";
                break;

            case Calendar.SEPTEMBER:
                dayStr = "Sep";
                break;

            case Calendar.OCTOBER:
                dayStr = "Oct";
                break;

            case Calendar.NOVEMBER:
                dayStr = "Nov";
                break;

            case Calendar.DECEMBER:
                dayStr = "Dec";
                break;
        }

        return dayStr;
    }


    public static String getTimeStamp(Date originalDate){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);
        String currentDateTime = dateFormat.format(new Date());
        return dateFormat.format(new Date());
    }


    public static String getTimeStamp() {
        // format yyyy-MM-dd HH:mm:ss";
        TimeZone  localTimeZone  = TimeZone.getDefault();
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getTimeStamp2() {
        // format yyyy-MM-dd;
        try {
            TimeZone  localTimeZone  = TimeZone.getDefault();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(localTimeZone);
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }

    public static Long getDateTimeByTime(String times){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);
        try {
            date1 = dateFormat.parse(times);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }
    public static Long getDateTime(){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);
        Date date = new Date();
        String todate = dateFormat.format(date);
        try {
            date1 = dateFormat.parse(todate);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }
    public static Long getDateTimeBy(String SDate){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);

        try {
            date1 = dateFormat.parse(SDate);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }

    public static Long getDateTimeByFromEvent(String SDate){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(localTimeZone);

        try {
            date1 = dateFormat.parse(SDate);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }

    public static boolean CekEventToday(String SDate){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        boolean hasil = false;
        Date date1 = null;
        long difference = 0;
        long difference2 = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(localTimeZone);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(localTimeZone);
        Date date2 = new Date();
        String today = dateFormat2.format(date2);
        try {
            date1 = dateFormat.parse(SDate);
            date2 = dateFormat.parse(today);
            difference = date1.getTime();
            difference2 = date2.getTime();
            if(difference==difference2){
                hasil = true;
            }else{
                hasil = false;
            }
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return hasil;
    }


    public static String ChangeDate(String Date){
        TimeZone  localTimeZone  = TimeZone.getDefault();
        String Result = null;
        String month = "";
        String Years = Date.substring(2,4);
        String Tgl = Date.substring(8,10);
        String Mon = Date.substring(5,7);
        if(Mon.equals("01")){
            month="Jan";
        }
        else if(Mon.equals("02")){
            month="Feb";
        }
        else if(Mon.equals("02")){
            month="Feb";
        }
        else if(Mon.equals("03")){
            month="Mar";
        }
        else if(Mon.equals("04")){
            month="Apr";
        }
        else if(Mon.equals("05")){
            month="May";
        }
        else if(Mon.equals("06")){
            month="Jun";
        }
        else if(Mon.equals("07")){
            month="Jul";
        }
        else if(Mon.equals("08")){
            month="Aug";
        }
        else if(Mon.equals("09")){
            month="Sep";
        }
        else if(Mon.equals("10")){
            month="Oct";
        }
        else if(Mon.equals("11")){
            month="Nov";
        }
        else if(Mon.equals("12")){
            month="Des";
        }
        Result = Tgl+"-"+month+"-"+Years;
        return Result;
    }


    public static String HitungDUration(String Date1, String Date2){

        String format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(Date1);
            toDate = sdf.parse(Date2);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long diff = toDate.getTime() - fromDate.getTime();
        String dateFormat="Duration: ";
        int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
        if(diffDays>0){
            dateFormat+=diffDays+" day ";
        }
        diff -= diffDays * (24 * 60 * 60 * 1000);

        int diffhours = (int) (diff / (60 * 60 * 1000));
        if(diffhours>0){
            dateFormat+=diffhours+" hour ";
        }
        diff -= diffhours * (60 * 60 * 1000);

        int diffmin = (int) (diff / (60 * 1000));
        if(diffmin>0){
            dateFormat+=diffmin+" min ";
        }
        diff -= diffmin * (60 * 1000);

        int diffsec = (int) (diff / (1000));
        if(diffsec>0){
            dateFormat+=diffsec+" sec";
        }
        System.out.println(dateFormat);
        String Result = dateFormat;
        return Result;
    }


}
