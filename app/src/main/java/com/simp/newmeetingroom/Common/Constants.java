package com.simp.newmeetingroom.Common;

import android.content.Context;
import android.preference.PreferenceManager;

import com.simp.newmeetingroom.Apps;
import com.simp.newmeetingroom.BuildConfig;


public class Constants {

    public static String PRODUCTIONAPI = "http://webapp.indoagri.co.id/Imobile/api/";
    public static String DEVELOPMENTAPI = "http://dev.indoagri.co.id/fajar/imobile/api/";
    public static String API            = "http://10.126.105.200:8814/android/api/";
    static Context context = Apps.getAppContext();


    public static  final String APIKEY = "0ooksgckckgokscw8ckook44kokow4040cgkoo8w";
    PreferenceManager preferenceManager;
    public static final String const_VersionName = DeviceUtils.getAppVersion(context);
    public static final String const_AppsName = DeviceUtils.getAppName(context);
    public static final String const_APIVERSION = String.valueOf(DeviceUtils.APIVERSION(context));
    public static final String shared_name = DeviceUtils.getAppName(context)+"Pref";
    public static final String db_name = DeviceUtils.getAppName(context)+".db";
    public static final String apk_name = DeviceUtils.getAppName(context)+".apk";
    public static final int db_version = BuildConfig.VERSION_CODE;
    public static final String versionApps = BuildConfig.VERSION_NAME;


    public static final String PAR_TYPE_SERVICE = "TYPE_SERVICE";
    public static final String PAR_DATA_SERVICE = "DATA_SERVICE";
    public static final String RESULT_CODE = "RESULT_CODE";

    public static final int ServiceGetDataNew = 100;
    public static final int ServiceBackToHome = 101;
    public static final int ServiceCheckInFunction = 102;
    public static final int ServiceCheckOutFunction = 103;
    public static final int ServiceBeverageFunction = 104;
    public static final int ServiceRoutine1Minute = 105;
    public static final int SendNotification = 106;

    public static final int StatusListenerThisRoom = 1000;
    public static final int ThisRoomCheckIn  = 10001;
    public static final int ThisRoomCheckOut = 10002;
    public static final int ThisRoomAttendees = 10003;

    public static final int StatusListenerOtherRoom = 1001;
}
