package com.simp.newmeetingroom.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.simp.newmeetingroom.R;

public class ViewDialog {

    Dialog dialog;
    Activity activity;
    String textMessage;
    public void ViewDialog(Dialog dialognew, Activity activity, String msg){
        this.dialog = dialognew;
        this.activity = activity;
        this.textMessage = msg;
        final boolean customTitleSupported = dialognew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognew.setCancelable(false);
        dialognew.setContentView(R.layout.dialog_connection);
        if(customTitleSupported){
            TextView text = (TextView) dialognew.findViewById(R.id.text_dialog);
            text.setText(textMessage);
        }
    }
    public void showDialog(){
        dialog.show();
    }
    public void dismissDialog(){
        if(dialog.isShowing()){
            dialog.dismiss();
        }


    }
}